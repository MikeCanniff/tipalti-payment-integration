/**
 * Module Description
 * 
 * This module will sync bank accounts with provider names in Tipalti. This is meant to run as a scheduled script to be run as needed
 * in client accounts. It will update a custom record in NetSuite that will be used as a dropdown during account setup. NetSuite
 * bank accounts are regular GL accounts with a type = BANK. There is a custom field on that record which will link to the list of
 * provider names updated via this script. 
 * 
 * The script calls a Tipalti web service which returns all provider names. This list is compared against the current list in the custom record. Any
 * new ones will be added
 * 
 * Version    Date            Author           	Remarks
 * 1.00       18 Sep 2015     Mike Canniff		Initial Release
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function getProviderScheduled(type) {
	var currentContext = nlapiGetContext();
	var serviceURL = currentContext.getSetting('SCRIPT', 'custscript_wsserviceurl_tipalti');
	var errmsg = '';
	var currentContext = nlapiGetContext();
	var curDate = new Date();
	var st = Math.round(curDate.getTime() / 1000);	// number of seconds since 1970
	var payerName = currentContext.getSetting('SCRIPT', 'custscript_payername_tipalti');
	var privateKey = currentContext.getSetting('SCRIPT', 'custscript_privatekey_tipalti'); 		//The secret key received from Tipalti QA account value
	var EAT = '';
	var soap = '';
	var eKey = payerName + st;			// this is the combined key that will be encrypted.

	serviceURL += 'PayerFunctions.asmx';
	errmsg = 'payer = ' + payerName + ' time stamp = ' + st + ' key = ' + privateKey + ' date timestamp = ' + curDate.getTime();
	nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', errmsg);
	eKey = Utf8.encode(eKey);
	var authString = CryptoJS.HmacSHA256(eKey, privateKey);  //incription 
	authString = CryptoJS.enc.Hex.stringify(authString);
	var errmsg = 'url = ' + serviceURL + ' complete key = ' + eKey + ' auth = ' + authString;
	nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', errmsg);

	soap = '<?xml version="1.0" encoding="UTF-8"?>\n';
	soap += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema�instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
	soap += '\t<soap:Body>';
    soap += '\t\t<GetProviderAccounts xmlns="http://Tipalti.org/">';
    soap += '\t\t\t<payerName>' + payerName + '</payerName>';
    soap += '\t\t\t<timestamp>' + st + '</timestamp>';
	soap += '\t\t\t<key>' + authString + '</key>';
    soap += '\t\t</GetProviderAccounts>';
	soap += '\t</soap:Body>';	   
	soap += '</soap:Envelope>';

	for (var i = 0; i < (Math.floor(soap.toString().length/3000)) + 1; i++) {
		var temp = soap.toString().substring(i*3000,i*3000 + 3000);
		if (i == 0) 
			temp = 'soap = ' + temp;		
		nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', temp);
	}
	

    var soapHead = new Array();
	soapHead['Content-Type'] = 'text/xml; charset=utf-8';
	var response = nlapiRequestURL(serviceURL, soap, soapHead);
	var soapText = response.getBody();

	if (response.getCode() != 200)	{
		var faultText = soapText;
		errmsg = 'Tipalti Response Code = ' + response.getCode() + ' fault text = ' + faultText;
		nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', errmsg);

	} else {
		var soapXML = nlapiStringToXML(soapText);
		logResponseXML(soapXML, 'soap:Body', 0);

		var errorNode = nlapiSelectNode(soapXML, "//*[name()='errorMessage']");
		var errorNodeMsg = null;
		var errorCode = nlapiSelectNode(soapXML, "//*[name()='errorCode']");
		var errorCodeMsg = null;
		// var errorMessage = errorNode.nodeValue;
		// even though the response XML has a good 200 error code, there still may be functional errors in the document
		// Need to search for nodes called errorMessage and errorCode. The actual value is saved at the first child.
		// Save the error values to the custom error field on the form.
		errmsg = 'Results ';
		if (errorNode) {
			errmsg += ' errorMessage = ' + errorNode.nodeValue;
			var childNode = errorNode.firstChild;
			if (childNode) {
				errmsg += ' value = ' + childNode.nodeValue;
				errorNodeMsg = childNode.nodeValue;
			}
		}
		if (errorCode) {
			errmsg += ' errorCode = ' + errorCode.nodeValue;
			var childNode = errorCode.firstChild;
			if (childNode) {
				errmsg += ' value = ' + childNode.nodeValue;
				errorCodeMsg = childNode.nodeValue;
			}
		}		
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
		if (errorNodeMsg || errorCodeMsg) { 
			if (errorNodeMsg != 'OK' && errorCodeMsg != 'OK') {
				nlapiSetFieldValue('custbody_wserrorcode_tipalti', errorNodeMsg + ' ' + errorCodeMsg);
				return;
			}
		}
		var providerNodes = nlapiSelectNodes(soapXML, "//*[name()='Provider']");
		if (providerNodes) {
			errmsg = 'provider nodes length = ' + providerNodes.length;
			nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', errmsg);
			for (var i = 0; i < providerNodes.length; i++) {
				var providerNode = providerNodes[i];
				var childNode = providerNode.firstChild;
				if (childNode) {
					var providerName = childNode.nodeValue;
					errmsg = 'Provider[' + i + '] = ' + providerName;
					nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', errmsg);
					var filter = new Array();
					filter[0] = new nlobjSearchFilter( 'name', null, 'is', providerName);
					// return the approval status and the transaction type
		//			var column = new Array();
		//			column[0] = new nlobjSearchColumn(  'custrecord_apacct_tipalti' );

					var results = nlapiSearchRecord( 'customrecord_bankname_tipalti', null, filter, null );
					if (results == null) {
						errmsg = 'New bank Account = ' + providerName;
						nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', errmsg);
						var record = nlapiCreateRecord('customrecord_bankname_tipalti');
						record.setFieldValue('name', providerName);
						try {
					    	var id = nlapiSubmitRecord( record );
					    	errmsg = 'Bank Name insert successful ID = ' + id;
							nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
					    } catch (e) {
					    	errmsg = e.getCode() + ' ' + e.getDetails();
							nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
							return {"Response" : errmsg};	
					    }	

					} else {
						errmsg = 'Found bank Account = ' + results[0].getId() + ' ' + providerName;
						nlapiLogExecution('DEBUG', 'Tipalti getProviderScheduled event', errmsg);
					}

				}
			}
			
		}

	}

	
}
