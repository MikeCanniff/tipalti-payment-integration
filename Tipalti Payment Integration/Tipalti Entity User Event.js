/**
 * Module Description
 * 
 * Version  Date            Author          Remarks
 * 1.00     28 Jul 2015     Mike
 * 1.01		02 Mar 2016		Mike			Added null condition checks for xml fields.
 * 1.02     15 Aug 2016		Keely			Added in if logic to check if there was change in the fields. If there was a change then XML is sent.
 * 1.03     17 Aug 2016		Keely			Added in companyname to the check on fields section.
 */

/**
 * 
 * This function needs to blank out the error message field on the vendor page for a re-submission. It also adds a custom field to the form
 * to represent the external ID. This field should be hidden. The custom external ID field will be set in the before submit event. The real externalid
 * field is set in the after submit event (the only place it works). It will copy the value from the custom field to the real externalid. The externalid
 * will sync with the web service primary key.
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function tipaltiVendorBeforeLoad(type, form, request){
	var errmsg = 'Type = ' + type;
	nlapiLogExecution('DEBUG', 'tipaltiVendorBeforeLoad event', errmsg);

	var currentContext = nlapiGetContext();
	var tipaltiInstallFlag = nlapiGetContext().getSetting('SCRIPT', 'custscript_installflag_tipalti');	

	if(tipaltiInstallFlag == 'T' && (type == 'create' || type == 'edit' || type == 'copy') && currentContext.getExecutionContext() == 'userinterface') {
		var errorField = nlapiGetField('custentity_wserrorcode_tipalti');
		// errorField.setLabel('');
		errorField.setDisplayType('disabled');
	}


}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function tipaltiVendorBeforeSubmit(type){

	var recType = nlapiGetRecordType().toLowerCase();
	var recId = nlapiGetRecordId();
	var currentContext = nlapiGetContext();
	var tipaltiInstallFlag = currentContext.getSetting('SCRIPT', 'custscript_installflag_tipalti');	
	var tipaltiOneWorldFlag = currentContext.getSetting('SCRIPT', 'custscript_oneworldflag_tipalti');	
	var serviceURL = currentContext.getSetting('SCRIPT', 'custscript_wsserviceurl_tipalti');
	var errorField = nlapiGetField('custentity_wserrorcode_tipalti');

	serviceURL += 'PayeeFunctions.asmx';
	var errmsg = 'Type = ' + type + ' rec type = ' + recType + ' ID = ' + nlapiGetRecordId() + ' exec context = ' + currentContext.getExecutionContext();
	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
	if(tipaltiInstallFlag == 'T' && (type != 'view') 
			&& (currentContext.getExecutionContext() == 'userinterface' || currentContext.getExecutionContext() == 'scheduled')) {

		var soap = '';
		var soap2 = '';
		var curDate = new Date();
		var st = Math.round(curDate.getTime() / 1000);	// get time returns number of milliseconds since 1970. Divide by 1000 to get seconds
		var stringDate = curDate.getFullYear() + '-' + (curDate.getMonth() + 1) + '-' + curDate.getDate();		// need to add 1 to the month, since it starts at zero
		var configData = nlapiLoadConfiguration('companyinformation');
		var syncOption = currentContext.getSetting('SCRIPT', 'custscript_vendorsyncpref_tipalti'); 		//The secret key received from Tipalti QA account value
		var payerName = currentContext.getSetting('SCRIPT', 'custscript_payername_tipalti');
		var privateKey = currentContext.getSetting('SCRIPT', 'custscript_privatekey_tipalti'); 		//The secret key received from Tipalti QA account value
		var adminEmail = currentContext.getSetting('SCRIPT', 'custscript_emailcontact_tipalti');
		var vendor = nlapiGetFieldValue('entityid');
//		var payeeIdap = nlapiGetFieldValue('entityid');		
		var payeeIdap = nlapiGetFieldValue('custentity_payeeid_tipalti');		
		var apAccount = nlapiGetFieldValue('payablesaccount');
		var EAT = nlapiGetFieldValue('billaddr1');
		var soapAction = '';

		errmsg = 'payer = ' + payerName + ' payee = ' + payeeIdap + ' key = ' + privateKey + ' sync option = ' + syncOption + ' AP account = ' + apAccount;
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
//		if (type == 'create' || type == 'copy') {
//		// var externalId = vendor + ' ' + nlapiDateToString(curDate, 'datetime');			// add a time stamp to make unique
//		var externalId = vendor;
//		payeeIdap = externalId;
//		errmsg = 'new external ID = ' + externalId;
//		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
//		nlapiSetFieldValue('custpage_externalid_tipalti', externalId);
//		} else {
//		nlapiSetFieldValue('custpage_externalid_tipalti', '');
//		payeeIdap = nlapiGetFieldValue('externalid');
//		}		
		if (payeeIdap == null || payeeIdap == '') {
			payeeIdap = '';						// make sure it is an empty string, not NULL
			soapAction = 'CreatePayeeInfoAutoIdap';
		} else {		
			payeeIdap = payeeIdap.substring(0, 49);
			soapAction = 'UpdateOrCreatePayeeInfo';
		}

		// Only Vendors marked with the Paid By Tipalti checkbox will be transferred to the other system.
		if (syncOption == '2' || syncOption == '4') {
			errmsg = 'This Vendor is NOT being synchronized with Tipalti. Sync option = ' + syncOption;
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
			return;
		}
		if (apAccount == '' || apAccount == null || nlapiLookupField('account', apAccount, 'custrecord_apacct_tipalti') == 'F') {
			errmsg = 'The current AP Account for this Vendor Bill is not setup or allowed for Tipalti = ' + apAccount;
			nlapiLogExecution('DEBUG', 'tipaltiPaymentBeforeSubmit event', errmsg);
			return;
		}

		if (type == 'delete') {
			return;
			// SPECIAL PROCESSING NEEDED FOR DELETING A VENDOR
		}

		errmsg = 'Final payer = ' + payerName + ' payee = ' + payeeIdap + ' time stamp = ' + st + ' key = ' + privateKey + ' date timestamp = ' + curDate.getTime();
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
		var eKey = payerName + payeeIdap + st + EAT;			// this is the combined key that will be encrypted.
		//eKey = CryptoJS.enc.Utf8.stringify(eKey);			// this returned a blank value
		eKey = Utf8.encode(eKey);
		errmsg = 'UTF key = ' + eKey;
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
		// var authString = SHA256(eKey);


		if (type != 'create') {
			var oldRecord = nlapiGetOldRecord();
			var noChange = true;	    	   
			if (((nlapiGetFieldValue('firstname') != oldRecord.getFieldValue('firstname')) &&  (((nlapiGetFieldValue('firstname') != '')) || (oldRecord.getFieldValue('firstname') != null))) )  { 
				// errmsg = 'there was a change in the field firstname. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('firstname')+ '. the old value is  = ' +oldRecord.getFieldValue('firstname'); 
				//nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			}
			if ((noChange != false && (nlapiGetFieldValue('lastname') != oldRecord.getFieldValue('lastname')) &&  ((nlapiGetFieldValue('lastname') != '') || (oldRecord.getFieldValue('lastname') != null)) ))  { 

			//	 errmsg = 'there was a change in the field  lastname. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('lastname')+ '. the old value is  = ' +oldRecord.getFieldValue('lastname'); 
			//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			}
			if ((noChange != false && (nlapiGetFieldValue('billaddr1') != oldRecord.getFieldValue('billaddr1')) &&  (((nlapiGetFieldValue('billaddr1') != '')) || (oldRecord.getFieldValue('billaddr1') != null))) )  { 
			//	 errmsg = 'there was a change in the field billaddr1. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('billaddr1')+ '. the old value is  = ' +oldRecord.getFieldValue('billaddr1'); 
			//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			}
			if ((noChange != false && (nlapiGetFieldValue('billaddr2') != oldRecord.getFieldValue('billaddr2')) &&  ((nlapiGetFieldValue('billaddr2') != '') || (oldRecord.getFieldValue('billaddr2') != null))) )  { 
			//	 errmsg = 'there was a change in the field billaddr2 . noChange = ' + noChange + ' the new value = ' +nlapiGetFieldValue('billaddr2')+ ' the old value is  = ' +oldRecord.getFieldValue('billaddr2'); 
			//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			}
			if ((noChange != false && (nlapiGetFieldValue('billcity') != oldRecord.getFieldValue('billcity')) &&  ((nlapiGetFieldValue('billcity') != '') || (oldRecord.getFieldValue('billcity') != null)) ))  { 
				 errmsg = 'there was a change in the field billcity. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('billcity')+ '. the old value is  = ' +oldRecord.getFieldValue('billcity'); 
				nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			}
			if ((noChange != false && (nlapiGetFieldValue('billstate') != oldRecord.getFieldValue('billstate')) &&  ((nlapiGetFieldValue('billstate') != '') || (oldRecord.getFieldValue('billstate') != null))) )  { 
			//	 errmsg = 'there was a change in the field billstate. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('billstate')+ '. the old value is  = ' +oldRecord.getFieldValue('billstate'); 
			//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			}
			if ((noChange != false && (nlapiGetFieldValue('billzip') != oldRecord.getFieldValue('billzip')) &&  ((nlapiGetFieldValue('billzip') != '') || (oldRecord.getFieldValue('billzip') != null))) )  { 
			//	 errmsg = 'there was a change in the field billzip. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('billzip')+ '. the old value is  = ' +oldRecord.getFieldValue('billzip'); 
			//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			}
			if ((noChange != false && (nlapiGetFieldValue('billcountry') != oldRecord.getFieldValue('billcountry')) &&  ((nlapiGetFieldValue('billcountry') != '') || (oldRecord.getFieldValue('billcountry') != null))) )  { 
			//	 errmsg = 'there was a change in the field billcountry. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('billcountry')+ '. the old value is  = ' +oldRecord.getFieldValue('billcountry'); 
			//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
			
			}
			if ((noChange != false && (nlapiGetFieldValue('email') != oldRecord.getFieldValue('email')) &&  ((nlapiGetFieldValue('email') != '') || (oldRecord.getFieldValue('email') != null)) ))  { 
			//	 errmsg = 'there was a change in the field email. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('email')+ '. the old value is  = ' +oldRecord.getFieldValue('email'); 
			//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				noChange = false;
		
			}
			if ((noChange != false && (nlapiGetFieldValue('companyname') != oldRecord.getFieldValue('companyname')) &&  ((nlapiGetFieldValue('companyname') != '') || (oldRecord.getFieldValue('companyname') != null)) ))  { 
				//	 errmsg = 'there was a change in the field email. noChange = ' + noChange + '. the new value = ' +nlapiGetFieldValue('companyname')+ '. the old value is  = ' +oldRecord.getFieldValue('companyname'); 
				//	nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
					noChange = false;
			}
		}

		if (noChange){
			 errmsg = 'there was no change in the required fields sent to Tipalti.'
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
			return;
		}	     
		var authString = CryptoJS.HmacSHA256(eKey, privateKey);
		authString = CryptoJS.enc.Hex.stringify(authString);
		 errmsg = 'url = ' + serviceURL + ' complete key = ' + eKey + ' auth = ' + authString;
		nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);

		soap  = '<?xml version="1.0" encoding="UTF-8"?>\n';
		soap  += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';		
		soap  += '\t<soap:Body>';
		soap  += '\t\t<' + soapAction + ' xmlns="http://Tipalti.org/">';
		soap += '\t\t\t<payerName>' + payerName + '</payerName>';
		soap += '\t\t\t<idap>' + payeeIdap + '</idap>';
		soap += '\t\t\t<timestamp>' + st + '</timestamp>';
		soap += '\t\t\t<key>' + authString + '</key>';
		if (nlapiGetFieldValue('firstname') != '' && nlapiGetFieldValue('firstname') != null)
			soap += '\t\t\t<firstName>' + nlapiGetFieldValue('firstname') + '</firstName>';
		if (nlapiGetFieldValue('lastname') != '' && nlapiGetFieldValue('lastname') != null)
			soap += '\t\t\t<lastName>' + nlapiGetFieldValue('lastname') + '</lastName>';
		if (nlapiGetFieldValue('billaddr1') != '' && nlapiGetFieldValue('billaddr1') != null)
			soap += '\t\t\t<street1>' + nlapiGetFieldValue('billaddr1') + '</street1>';
		if (nlapiGetFieldValue('billaddr2') != '' && nlapiGetFieldValue('billaddr2') != null)
			soap += '\t\t\t<street2>' + nlapiGetFieldValue('billaddr2') + '</street2>';
		if (nlapiGetFieldValue('billcity') != '' && nlapiGetFieldValue('billcity') != null)
			soap += '\t\t\t<city>' + nlapiGetFieldValue('billcity') + '</city>';
		if (nlapiGetFieldValue('billstate') != '' && nlapiGetFieldValue('billstate') != null)
			soap += '\t\t\t<state>' + nlapiGetFieldValue('billstate') + '</state>';
		if (nlapiGetFieldValue('billzip') != '' && nlapiGetFieldValue('billzip') != null)
			soap += '\t\t\t<zip>' + nlapiGetFieldValue('billzip') + '</zip>';
		if (nlapiGetFieldValue('billcountry') != '' && nlapiGetFieldValue('billcountry') != null)
			soap += '\t\t\t<country>' + nlapiGetFieldValue('billcountry') + '</country>';
		if (nlapiGetFieldValue('email') != '' && nlapiGetFieldValue('email') != null)
			soap += '\t\t\t<email>' + nlapiGetFieldValue('email') + '</email>';
		soap += '\t\t\t<company>' + nlapiGetFieldValue('companyname') + '</company>';

		soap += '\t\t</' + soapAction + '>';

		soap += '\t</soap:Body>';	   
		soap += '</soap:Envelope>';

		for (var i = 0; i < (Math.floor(soap.toString().length/3000)) + 1; i++) {
			var temp = soap.toString().substring(i*3000,i*3000 + 3000);
			if (i == 0) 
				temp = 'soap = ' + temp;		
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', temp);		
		}


		var soapHead = new Array();
		soapHead['Content-Type'] = 'text/xml; charset=utf-8';
		var response = nlapiRequestURL(serviceURL, soap, soapHead);
		var soapText = response.getBody();
		for (var i = 0; i < (Math.floor(soapText.toString().length/3000)) + 1; i++) {
			var temp = soapText.toString().substring(i*3000,i*3000 + 3000);
			if (i == 0) 
				temp = 'Response = ' + temp;		
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', temp);		
		}

		if (response.getCode() != 200)	{
			//var faultNode = nlapiSelectNode(soapXML, "//*[name()='faultstring']");
			//var faultText = 'Dear Admin - The following error has occurred:\n\n' 
			//	+ nlapiSelectValue( faultNode, "//*[name()='faultstring']");
			var faultText = soapText;
			errmsg = 'Tipalti Response Code = ' + response.getCode() + ' fault text = ' + faultText;
			nlapiLogExecution('ERROR', 'tipaltiBeforeSubmit event', errmsg);
			nlapiSetFieldValue('custentity_wserrorcode_tipalti', faultText);
			if (adminEmail != '' && adminEmail != null) {
				var subject = "Tipalti Before Submit Transaction Reported an error";
				var ccArray = new Array();
				ccArray[0] = adminEmail;
				nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);		
			}

		} else {
			var soapXML = nlapiStringToXML(soapText);
			logResponseXML(soapXML, 'soap:Body', 0);
			var errorNode = nlapiSelectNode(soapXML, "//*[name()='errorMessage']");
			var errorNodeMsg = null;
			var errorCode = nlapiSelectNode(soapXML, "//*[name()='errorCode']");
			var errorCodeMsg = null;
			// var errorMessage = errorNode.nodeValue;
			// even though the response XML has a good 200 error code, there still may be functional errors in the document
			// Need to search for nodes called errorMessage and errorCode. The actual value is saved at the first child.
			// Save the error values to the custom error field on the form.
			errmsg = 'Results ';
			if (errorNode) {
				errmsg += ' errorMessage = ' + errorNode.nodeValue;
				var childNode = errorNode.firstChild;
				if (childNode) {
					errmsg += ' value = ' + childNode.nodeValue;
					errorNodeMsg = childNode.nodeValue;
				}
			}
			if (errorCode) {
				errmsg += ' errorCode = ' + errorCode.nodeValue;
				var childNode = errorCode.firstChild;
				if (childNode) {
					errmsg += ' value = ' + childNode.nodeValue;
					errorCodeMsg = childNode.nodeValue;
				}
			}		
			nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
			if (errorNodeMsg || errorCodeMsg) {
				if (errorNodeMsg != 'OK' && errorCodeMsg != 'OK') {
					nlapiSetFieldValue('custentity_wserrorcode_tipalti', errorNodeMsg + ' ' + errorCodeMsg);
					if (adminEmail != '' && adminEmail != null) {
						var subject = "Tipalti Before Submit Transaction Reported an error";
						var ccArray = new Array();
						var faultText = 'Dear Admin - The following error has occurred:\n\n' 
							+  errorNodeMsg + ' ' + errorCodeMsg + ' for Vendor ' + vendor;
						ccArray[0] = adminEmail;
						nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);		
					}
					return;
				} else {
					nlapiSetFieldValue('custentity_wserrorcode_tipalti', 'Success');					
				}
//				nlapiSetFieldValue('custentity_payeeid_tipalti',nlapiGetFieldValue('companyname'));		// temporary fix until API returns a value.
				if (payeeIdap == '') {			// need to get the response IDAP from the create payee web service
					var payeeIdapNode = nlapiSelectNode(soapXML, "//*[name()='createdIdap']");
					payeeIdap = payeeIdapNode.firstChild.nodeValue;
					errmsg = 'Created payee IDAP = ' + payeeIdap;
					nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);					
					nlapiSetFieldValue('custentity_payeeid_tipalti',payeeIdap);							
				}

				// Now we need to update the custom external ID field in Tipalti. This requires a new web service call for custom fields.
				// need to reset the key since this call does not require an EAT parameter.
				// Also reset the soap action since this is a different web service API.

				eKey = payerName + payeeIdap + st;			// this is the combined key that will be encrypted.
				//eKey = CryptoJS.enc.Utf8.stringify(eKey);			// this returned a blank value
				eKey = Utf8.encode(eKey);
				errmsg = 'New UTF key for UpdatePayeeCustomFields = ' + eKey;
				nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);
				// var authString = SHA256(eKey);

				authString = CryptoJS.HmacSHA256(eKey, privateKey);
				authString = CryptoJS.enc.Hex.stringify(authString);
				errmsg = 'url = ' + serviceURL + ' complete key = ' + eKey + ' auth = ' + authString;
				nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);

				soapAction = 'UpdatePayeeCustomFields';
				soap  = '<?xml version="1.0" encoding="UTF-8"?>\n';
				soap  += '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';		
				soap  += '\t<soap:Body>';
				soap  += '\t\t<' + soapAction + ' xmlns="http://Tipalti.org/">';
				soap += '\t\t\t<payerName>' + payerName + '</payerName>';
				soap += '\t\t\t<idap>' + payeeIdap + '</idap>';
				soap += '\t\t\t<timestamp>' + st + '</timestamp>';
				soap += '\t\t\t<key>' + authString + '</key>';
				soap += '\t\t\t<customFieldsValues>';
				soap += '\t\t\t\t<KeyValuePair>';
				soap += '\t\t\t\t\t<Key>External ID</Key>';
				soap += '\t\t\t\t\t<Value>'+ nlapiGetFieldValue('companyname') + '</Value>';
				soap += '\t\t\t\t</KeyValuePair>';
				soap += '\t\t\t</customFieldsValues>';				
				soap += '\t\t</' + soapAction + '>';		    
				soap += '\t</soap:Body>';	   
				soap += '</soap:Envelope>';

				for (var i = 0; i < (Math.floor(soap.toString().length/3000)) + 1; i++) {
					var temp = soap.toString().substring(i*3000,i*3000 + 3000);
					if (i == 0) 
						temp = 'soap = ' + temp;		
					nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', temp);		
				}

				soapHead = new Array();
				soapHead['Content-Type'] = 'text/xml; charset=utf-8';
				response = nlapiRequestURL(serviceURL, soap, soapHead);
				soapText = response.getBody();

				if (response.getCode() != 200)	{
					//var faultNode = nlapiSelectNode(soapXML, "//*[name()='faultstring']");
					//var faultText = 'Dear Admin - The following error has occurred:\n\n' 
					//	+ nlapiSelectValue( faultNode, "//*[name()='faultstring']");
					var faultText = soapText;
					errmsg = 'Tipalti Response Code = ' + response.getCode() + ' fault text = ' + faultText;
					nlapiLogExecution('ERROR', 'tipaltiBeforeSubmit event', errmsg);
					nlapiSetFieldValue('custentity_wserrorcode_tipalti', faultText);
					if (adminEmail != '' && adminEmail != null) {
						var subject = "Tipalti Before Submit Transaction Reported an error";
						var ccArray = new Array();
						ccArray[0] = adminEmail;
						nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);		
					}

				} else {
					var soapXML = nlapiStringToXML(soapText);
					logResponseXML(soapXML, 'soap:Body', 0);
					var errorNode = nlapiSelectNode(soapXML, "//*[name()='errorMessage']");
					var errorNodeMsg = null;
					var errorCode = nlapiSelectNode(soapXML, "//*[name()='errorCode']");
					var errorCodeMsg = null;
					// var errorMessage = errorNode.nodeValue;
					// even though the response XML has a good 200 error code, there still may be functional errors in the document
					// Need to search for nodes called errorMessage and errorCode. The actual value is saved at the first child.
					// Save the error values to the custom error field on the form.
					errmsg = 'Results ';
					if (errorNode) {
						errmsg += ' errorMessage = ' + errorNode.nodeValue;
						var childNode = errorNode.firstChild;
						if (childNode) {
							errmsg += ' value = ' + childNode.nodeValue;
							errorNodeMsg = childNode.nodeValue;
						}
					}
					if (errorCode) {
						errmsg += ' errorCode = ' + errorCode.nodeValue;
						var childNode = errorCode.firstChild;
						if (childNode) {
							errmsg += ' value = ' + childNode.nodeValue;
							errorCodeMsg = childNode.nodeValue;
						}
					}		
					nlapiLogExecution('DEBUG', 'tipaltiBeforeSubmit event', errmsg);

					if (errorNodeMsg || errorCodeMsg) {
						if (errorNodeMsg != 'OK' && errorCodeMsg != 'OK') {
							nlapiSetFieldValue('custentity_wserrorcode_tipalti', errorNodeMsg + ' ' + errorCodeMsg);
							if (adminEmail != '' && adminEmail != null) {
								var subject = "Tipalti Before Submit Transaction Reported an error";
								var ccArray = new Array();
								var faultText = 'Dear Admin - The following error has occurred:\n\n' 
									+  errorNodeMsg + ' ' + errorCodeMsg + ' for Vendor ' + vendor;
								ccArray[0] = adminEmail;
								nlapiSendEmail('-5', adminEmail, subject, faultText, ccArray);		
							}
							return;
						} else {
							nlapiSetFieldValue('custentity_wserrorcode_tipalti', 'Success');					
						}
					}
				}
			}
		}

	}	


}

/**
 * 
 * THIS FUNCTION HAS BEEN DEPRACATED
 * 
 * This function will update the externalid of the record for create and copy modes only. This is the only place the record can be updated.
 * 
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function tipaltiVendorAfterSubmit(type){
	var errmsg = 'Type = ' + type;
	nlapiLogExecution('DEBUG', 'tipaltiVendorAfterSubmit', errmsg);

	var currentContext = nlapiGetContext();
	var tipaltiInstallFlag = nlapiGetContext().getSetting('SCRIPT', 'custscript_installflag_tipalti');	

	if(tipaltiInstallFlag == 'T' && (type == 'create' || type == 'copy') && currentContext.getExecutionContext() == 'userinterface') {
		var externalId = nlapiGetFieldValue('entityid');
		var recType = nlapiGetRecordType();
		var recId = nlapiGetRecordId();
		errmsg = 'Tipalti External ID = ' + externalId + ' old external = ' + nlapiGetFieldValue('externalid') + ' rectype = ' + recType + ' rec ID = ' + recId;
		nlapiLogExecution('DEBUG', 'tipaltiVendorAfterSubmit', errmsg);
//		nlapiSubmitField(recType, recId, 'externalid', externalId);
	}

}
