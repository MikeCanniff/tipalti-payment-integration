/**
 * 
 * Module Description
 * 
 * Version  Date            Author      Remarks
 * 1.00     26 Jan 2016     Mike		Initial Release
 * 1.01		16 Feb 2016		Mike		Changed payer fees function to use absolute value for amount field.
 * 1.02		17 Mar 2016		Mike		Needed to set the AP account when creating a payment. Otherwise, the incorrect default AP account may be used. 
 * 1.03		24 Mar 2016		Mike		Changed currency lookup to use the symbol field (ISO code) as filter.
 * 1.04		31 Mar 2016		Mike		Payee Details Changed IPN - Check for sync preferences. Also will now update the entityid regardless of new vendor or not.
 * 1.05		14 Jun 2016		Mike		Added support for inbound invoices. Also fixed bug to strip out embedded commas in the currency amounts.
 * 1.06		24 Aug 2016		Mike		Disappearing account default value when updating the account.
 * 1.07		30 Aug 2016		Mike		Fixed issue with disappearing account code. Case # 2477730
 * 1.08		13 Sep 2016		Mike		The payer fee IPN has a different field for reference code. Modified case statement to handle this.
 * 1.09		20 Sep 2016		Mike		Moved the account lookup code for the Bills IPN to that section of the switch statement. Also conditionaly 
 * 										return the account number based upon accounting preferences. (ZUMBA).
 * 1.10		29 Sep 2016		Mike		Added lookup for duplicate payment. If vendor payment already exists, based on ref code, then it will return this message.
 * 1.11		03 Oct 2016		Mike		Misc changes for supporting Bills IPN (mostly field name mapping).
 * 1.12		13 Oct 2016		Mike		Added code to set the session object prior to creating VB in NS. This will stop circular update.
 * 1.13		03 Nov 2016		Mike		Updated approval status code. Changed CDL mapping to use internal id and not name. 
 * 1.14		07 Nov 2016 	Mike		Added zip code and phone number field mapping for payee details change event.
 * 1.15		11 Nov 2016		Mike		Fixed issue with VB search results > 1000 for a specific vendor.
 * 1.16		22 Nov 2016		Mike		Made all RESTlet response JSON objects to start with 'Error: IPN Refcode ' + ipnData.refCode to standardize the return code.
 * 1.17		30 Nov 2016		Mike		Set the default VB status to approved on the payer fee IPN.
 * 1.18		14 Dec 2016		Mike		Fixed binary float conversion of Bills IPN on amounts with commas. 
 * 1.19		15 Dec 2016		Mike		Modified logic to submit payments. It will now ignore totals balancing between VB and IPN. It will apply the VBs 
 * 										on a FIFO basis in the applied list.
 * 1.20		03 Jan 2016		Mike		Added isinactive filter to the searchvendor function.
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getIPNRESTlet(dataIn) {
	nlapiLogExecution('DEBUG', 'tipalti get IPN event', 'Inside function parm = ' + dataIn.testparm);

	
	
	return {};
}

/**
 * 
 * This function accepts a post HTTP request. It assumes that the data is in JSON format as per NS RESTlet standard. It will parse
 * all input fields and validate fields. It then creates a NS vendor payment transaction. It will search for all vendor bills
 * passed in on the transaction. If any vendor bill does not exist or is already paid, it will return an error.
 * 
 * @param {object} dataIn Parameter in JSON format
 * @returns {Object} Output object
 */
function postIPNRESTlet(dataIn) {
	nlapiLogExecution('DEBUG', 'tipalti post IPN event', 'Inside function');
	var errmsg = '';
	var ipnData = new Object();
	var returnCode = {};
	var currentContext = nlapiGetContext();
	var accountingPref = nlapiLoadConfiguration('accountingpreferences');
	var classResults = null;
	var deptResults = null;
	var locationResults = null;
	var accountResults = null;
	
	var currentContext = nlapiGetContext();
	var useAccountNumber = accountingPref.getFieldValue('accountnumbers');
	ipnData.defaultLocation = currentContext.getSetting('SCRIPT', 'custscript_defaultlocation_tipalti');	
	ipnData.tipaltiVendorId = currentContext.getSetting('SCRIPT', 'custscript_vendorid_tipalti');	
	ipnData.payeeFeeAccount = currentContext.getSetting('SCRIPT', 'custscript_payeefees_tipalti');	
	ipnData.payerFeeAccount = currentContext.getSetting('SCRIPT', 'custscript_payerfees_tipalti');	
	ipnData.defaultSub = currentContext.getSetting('SCRIPT', 'custscript_defaultsub_tipalti');	
	ipnData.defaultAPAcct = currentContext.getSetting('SCRIPT', 'custscript_defaultapacct_tipalti');	
	ipnData.syncOption = currentContext.getSetting('SCRIPT', 'custscript_vendorsyncpref_tipalti'); 		//The secret key received from Tipalti QA account value

	if (ipnData.tipaltiVendorId == '' || ipnData.tipaltiVendorId == null) {
		errmsg = 'Missing Vendor ID for Tipalti Payer Fees';
		nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
		return {"Response" : errmsg};		
	}
	if (ipnData.defaultLocation == '' || ipnData.defaultLocation == null) {
		errmsg = 'Missing Default Location';
		nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
		return {"Response" : errmsg};		
	}
	if (ipnData.payeeFeeAccount == '' || ipnData.payeeFeeAccount == null) {
		errmsg = 'Missing Payee Fee Expense Account Location';
		nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
		return {"Response" : errmsg};		
	}
	if (ipnData.payerFeeAccount == '' || ipnData.payerFeeAccount == null) {
		errmsg = 'Missing Payer Fee Expense Account Location';
		nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
		return {"Response" : errmsg};		
	}
	if (currentContext.getSetting('SCRIPT', 'custscript_defaultlocation_tipalti') == 'T' ) {
		if (ipnData.defaultSub == '' || ipnData.defaultSub == null) {
			errmsg = 'Missing Default Subsidiary for Tipalti Payee Synch';
			nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
			return {"Response" : errmsg};		
		}
	}
	if (ipnData.defaultAPAcct == '' || ipnData.defaultAPAcct == null) {
		errmsg = 'Missing Default AP Account for Tipalti Payee Synch';
		nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
		return {"Response" : errmsg};		
	}
	// loop through all of the fields in the JSON post object.
	ipnData.vendorName = '';		// force to empty in case the fieldname is not sent in the IPN wrapper
	ipnData.bankName = '';
	ipnData.currencyName = '';
	ipnData.paymentMethod = '';
	ipnData.paymentCurrency = '';
	ipnData.apAccount = '';
	ipnData.payeeFees = 0.0;
	ipnData.payerFees = 0.0;
	ipnData.amount = 0.0;
	ipnData.vbList = '';
	ipnData.refCode = '';
	ipnData.externalId = '';
	ipnData.firstName = '';
	ipnData.lastName = '';
	ipnData.middleName = ''; 
	ipnData.company = ''; 
	ipnData.email = ''; 
	ipnData.street1 = ''; 
	ipnData.street2 = '';
	ipnData.city = '';
	ipnData.zip = '';
	ipnData.state = '';
	ipnData.country = ''; 
	ipnData.phone = '';
	ipnData.refCode = ''; 
	ipnData.feeCollectionDate = ''; 
	ipnData.description = ''; 
	ipnData.memo = '';

	for (var fieldName in dataIn) {
		if (dataIn.hasOwnProperty(fieldName)) {
			var value = dataIn[fieldName];
			errmsg = 'field = ' + fieldName + ' value = ' + value;
			nlapiLogExecution('DEBUG', 'tipalti post IPN event', errmsg);
			switch (fieldName) {
			case 'key' : ipnData.key = value; break;
			case 'c_date' : ipnData.cDate = value; break;
			case 'type' : ipnData.ipnType = value; break;
			case 'tipalti_fee_reference':			// use refCode property
			case 'ref_code' : ipnData.refCode = value; break;
			case 'payee_id' : ipnData.vendorName = value; break;
			case 'submit_date' : ipnData.submitDate = value; break;
			case 'cancelled_date' : ipnData.cancelDate = value; break;
			case 'amount_submitted' : ipnData.amount = parseFloat(value.replace(/,/g,'')); break;
			case 'currency_submitted' : ipnData.currencyName = value; break;
			case 'payment_currency' : ipnData.paymentCurrency = value; break;
			case 'payment_method' : ipnData.paymentMethod = value; break;
			case 'payee_fees' : ipnData.payeeFees = parseFloat(value.replace(/,/g,'')); break;
			case 'payer_fees' : ipnData.payerFees = parseFloat(value.replace(/,/g,'')); break;
			case 'External ID' : ipnData.externalId = value; break;				// this is a custom field in Tipalti that will need to be setup for each customer. It has a different format.
	//		case 'bank_name' : ipnData.bankName = value; break;
			case 'provider' : ipnData.bankName = value; break;
			case 'ap_account' : ipnData.apAccount = value; break;
			case 'related_invoice_ref_code' : ipnData.vbList = value; break;
			case 'error' : ipnData.error = value; break;
			case 'error_code' : ipnData.errorCode = value; break;
			case 'error_date' : ipnData.errorDate = value; break;
			// Vendor (Payee) Fields
			case 'first_name' : ipnData.firstName = value; break;
			case 'last_name' : ipnData.lastName = value; break;
			case 'middle_name' : ipnData.middleName = value; break;
			case 'company' : ipnData.company = value; break;
			case 'email' : ipnData.email = value; break;
			case 'street1' : ipnData.street1 = value; break;
			case 'street2' : ipnData.street2 = value; break;
			case 'city' : ipnData.city = value; break;
			case 'state' : ipnData.state = value; break;
			case 'zip_code' : ipnData.zip = value; break;
			case 'country' : ipnData.country = value; break;
			case 'phone' : ipnData.phone = value; break;
			case 'tax_id' : ipnData.taxIdNum = value; break;
			case '1099_applicable' : ipnData.is1099Eligible = value; break;
			case 'tax_form_type' : ipnData.taxFormType = value; break;
			case 'tax_form_entity_type' : ipnData.taxFormEntityType = value; break;
					// this will be the NetSuite Vendor ID.
			// Payer Fees fields
			case 'fee_collection_date' : ipnData.feeCollectionDate = value; break;
			case 'tipalti_fee_reference' : ipnData.feeReference = value; break;
			case 'fee_currency' : ipnData.currencyName = value; break;
			case 'fee_amount' : ipnData.payerFees = value; break;
			case 'fee_description' : ipnData.description = value; break;
			// Vendor Bill import fields
			case 'account' : ipnData.account = value; break;
			case 'bill_refcode' : ipnData.refCode = value; break;
			case 'bill_status' : ipnData.approvalStatus = value; break;
			case 'class' : ipnData.classification = value; break;
			case 'department' : ipnData.department = value; break;
			case 'location' : ipnData.location = value; break;
			case 'subsidiary' : ipnData.subsidiary = value; break;		// may not be needed.
			case 'bill_number' : ipnData.tranId = value; break;
			case 'bill_date' : ipnData.tranDate = value; break;
			case 'due_date' : ipnData.dueDate = value; break;
			case 'currency' : ipnData.currencyName = value; break;
			case 'description' : ipnData.memo = value; break;
			case 'lines_count' : ipnData.linesCount = value; break;

			
			default:
			}
			
		}
	}

	// validate the currency used. This will either be currencyName field or paymentCurrency field. This is required field and must exist in NS.
	if (ipnData.currencyName == '' && ipnData.paymentCurrency == '') {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Currency';
		nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
		return {"Response" : errmsg};
	} else {
		var filters = new Array();
		if (ipnData.currencyName == '')			// search by ISO code symbol
			filters[0] = new nlobjSearchFilter( 'symbol', null, 'is', ipnData.paymentCurrency );
		else
			filters[0] = new nlobjSearchFilter( 'symbol', null, 'is', ipnData.currencyName );
		var searchResults = nlapiSearchRecord( 'currency', null, filters, null);
		if (!searchResults) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Currency used = ' + ipnData.currencyName;
			nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
			return {"Response" : errmsg };	
		}
		ipnData.currencyId = searchResults[0].getId();
		errmsg = 'Found currency id = ' + ipnData.currencyId;
		nlapiLogExecution('DEBUG', 'tipalti post IPN event', errmsg);
	}	
	
	switch (ipnData.ipnType) {
	case 'payment_submitted':
	case 'completed':
		// validate the vendor info. Vendor is required and must exist in NS.
		if (ipnData.vendorName == '') {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Vendor/Payee';
			nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
			return {"Response" : errmsg};
		} else {
			searchVendor(ipnData);
			if (ipnData.vendorId == null) {
				errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Vendor Name = ' + ipnData.vendorName;
				nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
				return {"Response" : errmsg };	
			}
		}	

		returnCode = processSubmittedIpn(ipnData);
		break;
	case 'payment_cancelled':
	case 'error':
		// validate the vendor info. Vendor is required and must exist in NS.
		if (ipnData.vendorName == '') {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Vendor/Payee';
			nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
			return {"Response" : errmsg};
		} else {
			searchVendor(ipnData);
			if (ipnData.vendorId == null) {
				errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Vendor Name = ' + ipnData.vendorName;
				nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
				return {"Response" : errmsg };	
			}
		}	
		returnCode = processErrorIpn(ipnData);
		break;
	case 'payee_details_changed':
		searchVendor(ipnData);
		returnCode = processPayeeIpn(ipnData);
		break;
	case 'payer_fees':
		returnCode = processPayerFeesIpn(ipnData);
		break;
	case 'bills':
		// validate the vendor info. Vendor is required and must exist in NS.
		// load all segments into memory to minimize search governance.
		var column = new Array();
		column[0] = new nlobjSearchColumn( 'name' );
		if (currentContext.getFeature('classes') == true) {
			classResults = nlapiSearchRecord( 'classification', null, null, column);
		}
		if (currentContext.getFeature('departments') == true) {
			deptResults = nlapiSearchRecord( 'department', null, null, column);
		}
		if (currentContext.getFeature('locations') == true) {
			locationResults = nlapiSearchRecord( 'location', null, null, column);
		}
		column[1] = new nlobjSearchColumn( 'type' );
		if (useAccountNumber == 'T')
			column[2] = new nlobjSearchColumn( 'number' );
	    var acctArray = ['Expense', 'DeferExpense', 'OthExpense', 'FixedAsset', 'OthCurrAsset']; 
		var filter = new Array();
		filter.push(new nlobjSearchFilter('type', null, 'anyof', acctArray));
		accountResults = nlapiSearchRecord( 'account', null, filter, column);
		if (ipnData.vendorName == '') {
			errmsg = 'Missing Vendor/Payee';
			nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
			return {"Response" : errmsg};
		} else {
			searchVendor(ipnData);
			if (ipnData.vendorId == null) {
				errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Vendor Name = ' + ipnData.vendorName;
				nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
				return {"Response" : errmsg };	
			}
		}	
		var ctr = 0;
		ipnData.lines = new Array();
		// validate all of the segments used on the lines. 
		for (ctr = 0; ctr < ipnData.linesCount; ctr++) {
			var fieldName = 'account_number' + (ctr+1);			// Tipalti lines start at one
			var segFilter = new Array();
			ipnData.lines[ctr] = new Object();
			if (useAccountNumber == 'T' && dataIn.hasOwnProperty(fieldName)) {
				ipnData.lines[ctr].account = searchSegment(dataIn[fieldName], accountResults, 'account', true);
				if (isNaN(ipnData.lines[ctr].account)) {				// searchSegment can return an error message which is not a number
					return ipnData.lines[ctr].account;
				}
			} else {
				fieldName = 'account_name' + (ctr+1);
				if (dataIn.hasOwnProperty(fieldName)) {
					ipnData.lines[ctr].account = searchSegment(dataIn[fieldName], accountResults, 'account', false);
					if (isNaN(ipnData.lines[ctr].account)) {				// searchSegment can return an error message which is not a number
						return ipnData.lines[ctr].account;
					}
				} else {			
					errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing GL Account for line = ' + (ctr+1);
					nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
					return {"Response" : errmsg };			
				}
			}
			fieldName = 'amount_' + (ctr+1);
			if (dataIn.hasOwnProperty(fieldName)) {
				ipnData.lines[ctr].amount = parseFloat(dataIn[fieldName].replace(/,/g,''));
			} else {
				errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing invoice amount for line = ' + (ctr+1);
				nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
				return {"Response" : errmsg };			
			}
			fieldName = 'description_' + (ctr+1);
			if (dataIn.hasOwnProperty(fieldName)) {
				ipnData.lines[ctr].memo = dataIn[fieldName];
			} else {
				ipnData.lines[ctr].memo = '';
			}
			// 10-2016 Decided not to map the CDL fields
//			ipnData.lines[ctr].classification = '';
//			ipnData.lines[ctr].department = '';
//			ipnData.lines[ctr].location = '';
			fieldName = 'Classes_' + (ctr+1);
			if (dataIn.hasOwnProperty(fieldName) && classResults != null) {
				ipnData.lines[ctr].classification = searchSegment(dataIn[fieldName], classResults, 'classification', false);
				if (isNaN(ipnData.lines[ctr].classification)) {				// searchSegment can return an error message which is not a number
					return ipnData.lines[ctr].classification;
				}
				ipnData.lines[ctr].classification = dataIn[fieldName];
//				segFilter[0] = new nlobjSearchFilter( 'name', null, 'is', ipnData.classification);
//				var results = nlapiSearchRecord( 'classification', null, segFilter, null );
//				if (results == null) {
//					errmsg = 'Invalid Class Name = ' + ipnData.classification;
//					nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
//					return {"Response" : errmsg };	
//				}
			} else {
				ipnData.lines[ctr].classification = '';
			}
			fieldName = 'Department_' + (ctr+1);
			if (dataIn.hasOwnProperty(fieldName) && deptResults != null) {
				ipnData.lines[ctr].department = searchSegment(dataIn[fieldName], deptResults, 'department', false);
				if (isNaN(ipnData.lines[ctr].department)) {				// searchSegment can return an error message which is not a number
					return ipnData.lines[ctr].department;
				}
			} else {
				ipnData.lines[ctr].department = '';
			}
			fieldName = 'Locations_' + (ctr+1);
			if (dataIn.hasOwnProperty(fieldName) && locationResults != null) {
				ipnData.lines[ctr].location = searchSegment(dataIn[fieldName], locationResults, 'location', false);
				if (isNaN(ipnData.lines[ctr].location)) {				// searchSegment can return an error message which is not a number
					return ipnData.lines[ctr].location;
				}
			} else {
				ipnData.lines[ctr].location = '';
			}

		}
		returnCode = processInvoice(ipnData);
		break;
	default:
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid IPN type = ' + ipnData.type;
		nlapiLogExecution('ERROR', 'tipalti post IPN event', errmsg);
		return {"Response" : errmsg};		
	}

	errmsg = 'Return code JSON = ' + returnCode; 
	nlapiLogExecution('DEBUG', 'tipalti post IPN event', errmsg);
	return returnCode;
	//return {"Response" : responseValue};
}

/*
 * 
 * This function handles a standard IPN and creates a payment record in NetSuite. It also creates a VB for payer fees and a VC for payee fees. The VC
 * has to be submitted to the DB prior to creating the payment so that it will show up in the applied sublist. 
 * 
 * Input = ipnData object with all fields coming from the RESTlet.
 * Output = return code in JSON format.
 * 
 */

function processSubmittedIpn(ipnData) {
	var errmsg = '';
	var bankId = 0;
	var apAccount = '';
	var apAcctId = 0;
	var vbList = '';
	var responseValue = 'OK';
	var vbResults = null;
	var vendorCredit = null;
	var vendorBill = null;
	var vendorCreditId = 0;
	var vendorBillId = 0;
	var dArray = ipnData.cDate.split('.');		// remove milleseconds so that we can create a date object.
	var dObject = new Date(dArray[0]);
	var dString = nlapiDateToString(dObject);
	
	// Determine if this is a duplicate payment. IPNs can be resubmitted in a batch, this might result in duplicates
	var vpFilter = new Array();
	vpFilter[0] = new nlobjSearchFilter( 'entity', null, 'anyof', ipnData.vendorId);
	vpFilter[1] = new nlobjSearchFilter( 'custbody_refcode_tipalti', null, 'is', ipnData.refCode);
	var vpResults = nlapiSearchRecord( 'vendorpayment', null, vpFilter, null );
	if (vpResults != null) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Duplicate payment for Vendor = ' + ipnData.vendorName;
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg};		
	} 
	

	// validate the bank account info. Bank account is required and it must exist in NetSuite based upon the custom field value
	if (ipnData.bankName == '') {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Bank Account';
		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);

		return {"Response" : errmsg};
	} else {
		// first look up bank name in the custom record of allowable bank names. The corresponding internal ID will be used on the account definition
		var bankFilter = new Array();
		bankFilter[0] = new nlobjSearchFilter( 'name', null, 'is', ipnData.bankName);

		var bankResults = nlapiSearchRecord( 'customrecord_bankname_tipalti', null, bankFilter, null );
		if (bankResults == null) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Bank Name = ' + ipnData.bankName;
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg };	
		}
		// Now look up the GL account that is using this bank account.
		var acctFilter = new Array();
		acctFilter[0] = new nlobjSearchFilter( 'custrecord_bankacct_tipalti', null, 'anyof', bankResults[0].getId());

		var acctResults = nlapiSearchRecord( 'account', null, acctFilter, null );
		if (acctResults == null) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Bank Account = ' + ipnData.bankName;
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg };	
		}
		bankId = acctResults[0].getId();
		errmsg = 'Bank Account ID = ' + bankId;
		nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);

	}
	/*
	// validate the AP account info. It is a required field, it must exist in NetSuite and by marked as OK account to sync with
	if (ipnData.apAccount == '') {
		errmsg = 'Missing AP Account';
		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
		return {"Response" : errmsg };
	} else {
		var acctFilter = new Array();
		acctFilter[0] = new nlobjSearchFilter( 'name', null, 'is', ipnData.apAccount);
		// return the approval status and the transaction type
		var acctColumn = new Array();
		acctColumn[0] = new nlobjSearchColumn(  'custrecord_apacct_tipalti' );

		var acctResults = nlapiSearchRecord( 'account', null, acctFilter, acctColumn );
		if (acctResults == null) {
			errmsg = 'Invalid AP Account = ' + ipnData.apAccount;
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg };	
		}
		var allowFlag = acctResults[0].getValue('custrecord_apacct_tipalti');
		errmsg = 'After AP search account flag = ' + allowFlag;
		nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
		if (allowFlag != 'T') {
			errmsg = 'AP Account = ' + ipnData.apAccount + ' not supported';
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg };		
		}
		apAcctId = acctResults[0].getId();
		errmsg = 'AP Account ID = ' + apAcctId;
		nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
	}
*/	
    // next check to see if a vendor credit should be created for any payee related fees. 
    if (ipnData.payeeFees != 0.0) {
    	// record must be created in dynamic mode so that the default fields can be pre-populated with for that Vendor
    	vendorCredit = nlapiCreateRecord('vendorcredit', {recordmode: 'dynamic'});		
    	
    	if (vendorCredit == null) {
    		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not create Vendor Credit transaction.';
    		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
    		return {"Response" : errmsg};
    	}
    	try {
	    	vendorCredit.setFieldValue('entity', ipnData.vendorId);
	    	vendorCredit.setFieldValue('currency', ipnData.currencyId);
	    	vendorCredit.setFieldValue('exchangerate', 1.0);
	   // 	vendorCredit.setFieldValue('location', ipnData.defaultLocation);
	    	vendorCredit.setFieldValue('custbody_ipn_generated_bill_tipalti', ipnData.refCode);		// set the reference code on the VC to link back to payment.
	    	vendorCredit.selectNewLineItem('expense');			// must select the current line first before any edits can occur
			vendorCredit.setCurrentLineItemValue('expense', 'amount', ipnData.payeeFees);
			vendorCredit.setCurrentLineItemValue('expense', 'account', ipnData.payeeFeeAccount);
			vendorCredit.commitLineItem('expense');
	    } catch (e) {
	    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Vendor Credit ' + e.getCode() + ' ' + e.getDetails();
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg};	
	    }	

    }

    // next check to see if a vendor bill should be created for any payer related fees. 
    if (ipnData.payerFees != 0.0) {
    	// record must be created in dynamic mode so that the default fields can be pre-populated with for that Vendor
    	vendorBill = nlapiCreateRecord('vendorbill', {recordmode: 'dynamic'});		
    	
    	if (vendorBill == null) {
    		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not create Vendor Bill transaction.';
    		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
    		return {"Response" : errmsg};
    	}
    	try {
	    	vendorBill.setFieldValue('entity', ipnData.tipaltiVendorId);
	    	vendorBill.setFieldValue('currency', ipnData.currencyId);
	    	vendorBill.setFieldValue('exchangerate', 1.0);
	    //	vendorBill.setFieldValue('location', ipnData.defaultLocation);
	     //  	vendorBill.setFieldValue('custbody_ipn_generated_bill_tipalti', 'T');		// check the box to show that this vendor bill was from the IPN
	       	vendorBill.setFieldValue('custbody_ipn_generated_bill_tipalti', ipnData.refCode);		// set the reference code on the VB to link back to payment.
	    	vendorBill.selectNewLineItem('expense');			// must select the current line first before any edits can occur
	    	vendorBill.setCurrentLineItemValue('expense', 'amount', ipnData.payerFees);
	    	vendorBill.setCurrentLineItemValue('expense', 'account', ipnData.payerFeeAccount);		
	    	vendorBill.commitLineItem('expense');
	    } catch (e) {
	    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Vendor Bill ' + e.getCode() + ' ' + e.getDetails();
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg};	
	    }	
   	
    }

	// Validate vendor bills. Bills are required. And the number of bills on the input list must exist in NetSuite
	if (ipnData.vbList == '') {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Vendor Bill (Invoice) List';
		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
		return {"Response" : errmsg};
	} else {
		var vbArray = ipnData.vbList.split(',');
		var total = 0.0;
		errmsg = 'VB Array length = ' + vbArray.length;
		nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
		vbResults = searchVB(vbArray, ipnData.vendorId);
		if (vbResults == null) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' No Vendor Bill (Invoice) Found or Invalid AP Account Used';
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg};
		}
		if (vbArray.length != vbResults.length) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Mismatch on Vendor Bill (Invoice) List. IPN bill count = ' + vbArray.length + ' VB search count = ' + vbResults.length;
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg};	
		}
//		for (var i = 0; i < vbResults.length;i++) {
//			total += parseFloat(vbResults[i].amount);
//			apAcctId = vbResults[i].apAccount;
//			errmsg = 'VB ID[' + i + '] = ' + vbResults[i].internalId + ' amount = ' + vbResults[i].amount + ' total = ' + total + ' AP account = ' + apAcctId;
//			nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
//		}
//		if (ipnData.amount != total - ipnData.payeeFees) {
//			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Vendor bill amounts do not equal payment amount. IPN amount(' + ipnData.amount + ') = VB total(' + total + ') - Fees(' + ipnData.payeeFees + ').';
//			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
//			return {"Response" : errmsg};					
//		}

	}
	// submit the vendor credit as late as possible in the process. The vendor credit must be created prior to the payment record so 
	// that it will show up on the payment apply list. Note - surrounding the entire code with try catch did not seem to work. The submit
	// record was still committed even if other errors occurred. No rollback happens. So if any more errors occur, this VC must be deleted.
	
	if (vendorCredit) {
		try {
	    	vendorCreditId = nlapiSubmitRecord( vendorCredit );
	    	errmsg = 'Vendor Credit insert successful ID = ' + vendorCreditId;
			nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
	    } catch (e) {
	    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg};	
	    }	
	}
	
	// record must be created in dynamic mode so that the apply sublist can be pre-populated with all VBs and VCs for that Vendor
//	var payment = nlapiCreateRecord('vendorpayment', {recordmode: 'dynamic'});		
	var payment = nlapiCreateRecord('vendorpayment', {recordmode: 'dynamic', entity: ipnData.vendorId});		
	
	if (payment == null) {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not create Vendor Payment transaction.';
		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
		return {"Response" : errmsg};
	}
	
	errmsg = 'Create payment record account = ' + payment.getFieldValue('account') + ' bankid = ' + bankId + ' sub = ' + payment.getFieldValue('subsidiary');
	nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
	
	// set all of the required header values
//	payment.setFieldValue('entity', ipnData.vendorId);
//	payment.setFieldValue('subsidiary', ipnData.subsidiary);
//	payment.setFieldValue('apacct', apAcctId );
	payment.setFieldValue('currency', ipnData.currencyId);
	payment.setFieldValue('trandate', dString);
//	payment.setFieldValue('externalid', ipnData.refCode);
	payment.setFieldValue('custbody_refcode_tipalti', ipnData.refCode);
//	payment.setFieldValue('tranid', ipnData.refCode);				// 8-30-16 decided not to update the transaction id, since this is autogenerated.
	payment.setFieldValue('exchangerate', 1.0);
	payment.setFieldValue('account', bankId);						// account needs to be the last field updated since currency update resets this field.
//	errmsg = 'after exchange rate account = ' + payment.getFieldValue('account') + ' AP account = ' + payment.getFieldValue('apacct') 
//		+ ' sub = ' + payment.getFieldValue('exchangerate') + ' entity = ' + payment.getFieldValue('entity')
//		+ ' trandate = ' + payment.getFieldValue('trandate');
//	nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
	
	if (vendorCreditId != 0) {
		var found = false;
		// find the vendor credit that was just created in the apply sublist via internal IDs.
		for (var j = 1; j <= payment.getLineItemCount('apply'); j++) {
//			errmsg = 'Apply VB ID[' + j + '] = ' + payment.getLineItemValue('apply', 'internalid', j) + ' amount = ' + payment.getLineItemValue('apply', 'amount', j);
//			nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
			if (payment.getLineItemValue('apply', 'internalid', j) == vendorCreditId) {
				found = true;
				payment.selectLineItem('apply', j);			// must select the current line first before any edits can occur
				payment.setCurrentLineItemValue('apply', 'apply', 'T');
				payment.commitLineItem('apply');
				break;
			}
		}
		if (!found) {
			errmsg = 'Could not find vendor credit = ' + vendorCreditId + ' for this Apply list.';
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			// clean up the previously submitted vendor credit.
			try {
				nlapiDeleteRecord('vendorcredit', vendorCreditId);
				nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', 'Deleting Vendor Credit since it was not found.');
		    } catch (e) {
		    	errmsg = e.getCode() + ' ' + e.getDetails();
				nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
				return {"Response" : errmsg};	
		    }	
			return {"Response" : errmsg};			
		}
	}
	
	// loop through all of the vendor bills listed on the IPN
	errmsg = 'New payment apply count = ' +  payment.getLineItemCount('apply');
	nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
	
	var remainingAmount = ipnData.amount;
	for (var i = 0; remainingAmount > 0 && i < vbResults.length;i++) {
		var found = false;
		var vbAmount = 0.0;
		// find the matching VB in the apply sublist via internal IDs.
		for (var j = 1; remainingAmount > 0 && j <= payment.getLineItemCount('apply'); j++) {
			errmsg = 'VB ID[' + i + '] = ' + vbResults[i].internalId + ' Apply VB ID[' + j + '] = ' + payment.getLineItemValue('apply', 'internalid', j);
			nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
			if (payment.getLineItemValue('apply', 'internalid', j) == vbResults[i].internalId) {
				vbAmount = parseFloat(vbResults[i].amount);
				found = true;
				payment.selectLineItem('apply', j);			// must select the current line first before any edits can occur
				if (vbAmount <= remainingAmount) {
					payment.setCurrentLineItemValue('apply', 'amount', vbAmount);
					remainingAmount -= vbAmount;
				} else {
					payment.setCurrentLineItemValue('apply', 'amount', remainingAmount);
					remainingAmount = 0.0;				
				}
				payment.setCurrentLineItemValue('apply', 'apply', 'T');
	//				payment.setCurrentLineItemValue('apply', 'applydate', cDate);
	//				payment.setCurrentLineItemValue('apply', 'internalid', vbResult.getId());
				payment.commitLineItem('apply');
				break;
			}
		}
		if (!found) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not find vendor bill = ' + vbResults[i].internalId + ' for this Apply list.';
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			if (vendorCreditId != 0) {
				try {
					nlapiDeleteRecord('vendorcredit', vendorCreditId);
			    } catch (e) {
			    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
					nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
					return {"Response" : errmsg};	
			    }
			}
//			throw new nlobjError('IPN ERROR', errmsg);			// decided not to use the THROW feature since the record gets saved anyways
			return {"Response" : errmsg};			
		}
	}
	
	// submit vendor bill for payer fees. If this fails, need to roll back the vendor credit
	if (vendorBill) {
		try {
	    	vendorBillId = nlapiSubmitRecord( vendorBill );
	    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Vendor Bill insert successful ID = ' + vendorBillId;
			nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
	    } catch (e) {
	    	errmsg = e.getCode() + ' ' + e.getDetails();
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			// rollback the vendor credit
			if (vendorCreditId != 0) {
				try {
					nlapiDeleteRecord('vendorcredit', vendorCreditId);
			    } catch (e) {
			    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
					nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
					return {"Response" : errmsg};	
			    }	
			}
			return {"Response" : errmsg};	
	    }	
	}
	
	errmsg = 'Prior to submit record account = ' + payment.getFieldValue('account') + ' ' +  payment.getFieldText('account') + ' ap = ' + payment.getFieldValue('apacct');
	nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
	try {
    	var paymentId = nlapiSubmitRecord( payment );
    	errmsg = 'Vendor Payment insert successful ID = ' + paymentId;
		nlapiLogExecution('DEBUG', 'tipalti processSubmittedIpn', errmsg);
    } catch (e) {
    	errmsg = e.getCode() + ' ' + e.getDetails();
		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
		// clean up the previously submitted vendor credit and vendor bill.
		if (vendorCreditId != 0) {
			try {
				nlapiDeleteRecord('vendorcredit', vendorCreditId);
		    } catch (e) {
		    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
				nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
				return {"Response" : errmsg};	
		    }	
		}
		if (vendorBillId != 0) {
			try {
				nlapiDeleteRecord('vendorbill', vendorBillId);
		    } catch (e) {
		    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
				nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
				return {"Response" : errmsg};	
		    }	
		}
		return {"Response" : errmsg};	
    }	
    
	return {"Response" : "OK"};
}

/*
 * 
 * This function handles a ERROR IPN. It will void the vendor payment. Depending on accounting preference defined in NS, it will
 * either void related vendor bills and vendor credits. Or delete these.
 *  
 * Input = ipnData object with all fields coming from the RESTlet.
 * Output = return code in JSON format.
 * 
 */

function processErrorIpn(ipnData) {
	var errmsg = '';
	var paymentId = 0;
	var vbId = 0;
	var vcId = 0;
	var config = nlapiLoadConfiguration('accountingpreferences');		
	var reversalFlag = config.getFieldValue('reversalvoiding');
	
	if (ipnData.refCode == '') {
		errmsg = 'Error: Missing Vendor Payment Reference Code';
		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
		return {"Response" : errmsg};		
	}
	errmsg = 'Error code = ' + ipnData.errorCode + ' text = ' + ipnData.error + ' payment ID = ' + ipnData.refCode + ' reversal flag = ' + reversalFlag;
	nlapiLogExecution('DEBUG', 'tipalti processErrorIpn', errmsg);

	var vpFilter = new Array();
	vpFilter[0] = new nlobjSearchFilter( 'custbody_refcode_tipalti', null, 'is', ipnData.refCode);
	vpFilter[1] = new nlobjSearchFilter( 'mainline', null, 'is', 'T');
	// return the approval status and the transaction type
	var vpColumn = new Array();
	vpColumn[0] = new nlobjSearchColumn(  'entity' );
	vpColumn[1] = new nlobjSearchColumn(  'amount' );
	var vpResults = nlapiSearchRecord( 'vendorpayment', null, vpFilter, null );
	// now look up vendor bills associated with this payment for payer fees
	var vbFilter = new Array();
	vbFilter[0] = new nlobjSearchFilter( 'custbody_ipn_generated_bill_tipalti', null, 'is', ipnData.refCode);
	vbFilter[1] = new nlobjSearchFilter( 'mainline', null, 'is', 'T');
	// return the approval status and the transaction type
	var vbColumn = new Array();
	vbColumn[0] = new nlobjSearchColumn(  'entity' );
	vbColumn[1] = new nlobjSearchColumn(  'amount' );
	var vbResults = nlapiSearchRecord( 'vendorbill', null, vbFilter, null );
	// now look up vendor credits associated with this payment for payee fees.
	var vcFilter = new Array();
	vcFilter[0] = new nlobjSearchFilter( 'custbody_ipn_generated_bill_tipalti', null, 'is', ipnData.refCode);
	vcFilter[1] = new nlobjSearchFilter( 'mainline', null, 'is', 'T');
	// return the approval status and the transaction type
	var vcColumn = new Array();
	vcColumn[0] = new nlobjSearchColumn(  'entity' );
	vcColumn[1] = new nlobjSearchColumn(  'amount' );
	var vcResults = nlapiSearchRecord( 'vendorcredit', null, vcFilter, null );
	errmsg = 'Afer search VP = ' + vpResults + ' VB = ' + vbResults + ' VC = ' + vcResults;
	nlapiLogExecution('DEBUG', 'tipalti processErrorIpn', errmsg);
	if (vpResults) {
		paymentId = vpResults[0].getId();
		if (vbResults)
			vbId = vbResults[0].getId();
		if (vcResults)
			vcId = vcResults[0].getId();
		errmsg = 'payment id = ' + paymentId + ' bill id = ' + vbId + ' credit id = ' + vcId;
		nlapiLogExecution('DEBUG', 'tipalti processErrorIpn', errmsg);
		try {
			nlapiVoidTransaction('vendorpayment', paymentId);
			if (vbId != 0)
				if (reversalFlag == 'F')
					nlapiVoidTransaction('vendorbill', vbId);
				else
					nlapiDeleteRecord('vendorbill', vbId);
			if (vcId != 0)
				if (reversalFlag == 'F')
					nlapiVoidTransaction('vendorcredit', vcId);
				else
					nlapiDeleteRecord('vendorcredit', vcId);					
	    } catch (e) {
	    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			return {"Response" : errmsg};	
	    }	

	//	nlapiSubmitField('vendorpayment', paymentId,'status', 'Voided');
	} else {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not find vendor payment = ' + ipnData.refCode;
		nlapiLogExecution('DEBUG', 'tipalti processErrorIpn', errmsg);
		return {"Response" : errmsg};	
	}
	
	
	
	return {"Response" : "OK"};
}

/*
 * This function will handle the IPN case for updating a vendor record. The vendor will already have 
 * been searched for and found based upon the payee_id field. So it will load this record, update the 
 * appropriate fields and submit the record. 
 */


function processPayeeIpn(ipnData) {
	var errmsg = '';
	var bankId = 0;
	var apAccount = '';
	var vbList = '';
	var responseValue = 'OK';
	var vendor = null;
	var newVendor = false;

//	searchVendor(ipnData);
	if (ipnData.syncOption == '3' || ipnData.syncOption == '4') {
    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + 'Tipalti Sync Option = ' + ipnData.syncOption + '. This option does not allow vendor update from Tipaltu to NetSuite ';
		nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
		return {"Response" : errmsg};	
		
	}
	if (ipnData.vendorId == null) {
		vendor = nlapiCreateRecord('vendor');
		errmsg = 'new vendor created' + vendor.getId() ;
		nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
		newVendor = true;
	} else {
		vendor = nlapiLoadRecord('vendor', ipnData.vendorId, {recordmode: 'dynamic'});	
	}
	errmsg = 'New vendor flag = ' + newVendor;
	nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
	
	if (vendor == null) {
    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode +  'Could not load Vendor = ' + ipnData.vendorId + ' ' + ipnData.vendorName;
		nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
		return {"Response" : errmsg};	
	}
	vendor.setFieldValue('autoname', 'T');
	// entityid will be company name + payee id. So the auto flag must be turned off on vendors created in Tipalti
	if (ipnData.company == '') {
		vendor.setFieldValue('isperson', 'T');
		vendor.setFieldValue('firstname', ipnData.firstName);
		vendor.setFieldValue('middlename', ipnData.middleName);
		vendor.setFieldValue('lastname', ipnData.lastName);
//		if (!newVendor)			// 3/31/16 decided to always update the entity id
//			vendor.setFieldValue('entityid', ipnData.firstName + ' ' + ipnData.lastName + ' '+ ipnData.vendorName);
	} else {
		vendor.setFieldValue('isperson', 'F');		
		vendor.setFieldValue('companyname', ipnData.company);
		vendor.setFieldValue('legalname', ipnData.company);
//		if (!newVendor)
//			vendor.setFieldValue('entityid', ipnData.company + ' ' + ipnData.vendorName);
	}
	vendor.setFieldValue('email', ipnData.email);
	vendor.setFieldValue('phone', ipnData.phone);
	vendor.setFieldValue('custentity_payeeid_tipalti', ipnData.vendorName);
	// the 1099 status is set based on two input fields. If the 1099 eligible field is mapped, then that take precedence.
	if (ipnData.hasOwnProperty('is1099Eligible')) {
		errmsg = '1099 eligible = ' + ipnData.is1099Eligible;
		nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
		if (ipnData.is1099Eligible.toUpperCase() == 'TRUE')
			vendor.setFieldValue('is1099eligible', 'T');
		else
			vendor.setFieldValue('is1099eligible', 'F');	
	} else if (ipnData.hasOwnProperty('taxFormType') && ipnData.hasOwnProperty('taxFormEntityType')) {
		errmsg = 'Tax form type = ' + ipnData.taxFormType + ' entity type = ' + ipnData.taxFormEntityType;
		nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
		if (ipnData.taxFormType == 'W9' && (ipnData.taxFormEntityType.toUpperCase() == 'INDIVIDUAL' || ipnData.taxFormEntityType.toUpperCase() == 'PARTNERSHIP'))
			vendor.setFieldValue('is1099eligible', 'T');
		else
			vendor.setFieldValue('is1099eligible', 'F');	
	}
	vendor.setFieldValue('taxidnum', ipnData.taxIdNum);
	if (newVendor) {
		errmsg = 'new address default line ';
		nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
		vendor.setFieldValue('subsidiary', ipnData.defaultSub);
		vendor.setFieldValue('payablesaccount', ipnData.defaultAPAcct);
		vendor.selectNewLineItem('addressbook');
		vendor.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');  //This field is not a subrecord field.
		vendor.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');   //This field is not a subrecord field.
		var subrecord = vendor.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		if (subrecord == null) {
	    	errmsg = 'Could not load new address subrecord = ' + ipnData.vendorName;
			nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
			return {"Response" : errmsg};	
		}
		
		subrecord.setFieldValue('country', ipnData.country);			// need to set the country code first.
		subrecord.setFieldValue('addr1', ipnData.street1);
		subrecord.setFieldValue('addr2', ipnData.street2);
		subrecord.setFieldValue('city', ipnData.city);
		subrecord.setFieldValue('state', ipnData.state);
		subrecord.setFieldValue('zip', ipnData.zip);
		subrecord.setFieldValue('addrphone', ipnData.phone);
		subrecord.setFieldValue('addressee', ipnData.company);
		subrecord.commit();			// need to commit the subrecord prior to the line item
		vendor.commitLineItem('addressbook');
		
	} else {		
	// loop through all of the addresses for the vendor until the default billing is found. Then load that subrecord for updating.
		for (var i = 1; i <= vendor.getLineItemCount('addressbook'); i++) {
			if (vendor.getLineItemValue('addressbook', 'defaultbilling', i) == 'T') {
				errmsg = 'address default line = ' + i;
				nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
				vendor.selectLineItem('addressbook', i);
				var subrecord = vendor.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
				if (subrecord == null) {
			    	errmsg = 'Could not load address subrecord = ' + ipnData.vendorName + ' line = ' + i;
					nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
					return {"Response" : errmsg};	
				
				}
				subrecord.setFieldValue('country', ipnData.country);			// need to set the country code first.
				subrecord.setFieldValue('addr1', ipnData.street1);
				subrecord.setFieldValue('addr2', ipnData.street2);
				subrecord.setFieldValue('city', ipnData.city);
				subrecord.setFieldValue('state', ipnData.state);
				subrecord.setFieldValue('zip', ipnData.zip);
				subrecord.setFieldValue('addrphone', ipnData.phone);
				subrecord.setFieldValue('addressee', ipnData.company);
				subrecord.commit();			// need to commit the subrecord prior to the line item
				vendor.commitLineItem('addressbook');
				break;
			}
		}
	}
	
	try {
    	var vendorId = nlapiSubmitRecord( vendor );
    	errmsg = 'Vendor insert successful ID = ' + vendorId;
		nlapiLogExecution('DEBUG', 'tipalti processPayeeIpn', errmsg);
    } catch (e) {
    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
		nlapiLogExecution('ERROR', 'tipalti processPayeeIpn', errmsg);
		// rollback the vendor credit
		return {"Response" : errmsg};	
    }	

	return {"Response" : "OK"};	
}

/*
 * This function will handle the IPN case for processing payer fees. It will create a new VB for payments to be made to the 
 * vendor representing Tipalti. These are fees owed by the organization to Tipalti. So the vendor ID is saved in the 
 * company preferences. It will also create a Vendor Payment for this VB. This will result in a Fully Paid VB.
 *   
 */


function processPayerFeesIpn(ipnData) {
	var errmsg = '';
	var bankId;
	var apAccount = '';
	var dArray = ipnData.cDate.split('.');		// remove milleseconds so that we can create a date object.
	var dObject = new Date(dArray[0]);
	var dString = nlapiDateToString(dObject);
	var vbList = '';
	var vendorBillId = 0;
	var vendorBill = nlapiCreateRecord('vendorbill', {recordmode: 'dynamic'});		
	
	if (vendorBill == null) {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not create Vendor Bill transaction.';
		nlapiLogExecution('ERROR', 'tipalti processPayerFeesIpn', errmsg);
		return {"Response" : errmsg};
	}
	ipnData.tipaltiVendorId = nlapiGetContext().getSetting('SCRIPT', 'custscript_vendorid_tipalti');	
	ipnData.defaultLocation = nlapiGetContext().getSetting('SCRIPT', 'custscript_defaultlocation_tipalti');	
	ipnData.payerFeeAccount = nlapiGetContext().getSetting('SCRIPT', 'custscript_payerfees_tipalti');	
	
	if (ipnData.tipaltiVendorId == '' || ipnData.tipaltiVendorId == null) {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Vendor ID for Tipalti Payer Fees';
		nlapiLogExecution('ERROR', 'tipalti processPayerFeesIpn', errmsg);
		return {"Response" : errmsg};		
	}
	if (ipnData.defaultLocation == '' || ipnData.defaultLocation == null) {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Default Location';
		nlapiLogExecution('ERROR', 'tipalti processPayerFeesIpn', errmsg);
		return {"Response" : errmsg};		
	}
	if (ipnData.payerFeeAccount == '' || ipnData.payerFeeAccount == null) {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Payer Fee Expense Account Location';
		nlapiLogExecution('ERROR', 'tipalti processPayerFeesIpn', errmsg);
		return {"Response" : errmsg};		
	}
	bankId = searchBank(ipnData);
	if (isNaN(bankId)) {				// searchbank can return an error message which is not a number
		return bankId;
	}
	if (ipnData.refCode == '' || ipnData.refCode == null) {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Payer Fee Reference Code';
		nlapiLogExecution('DEBUG', 'tipalti processPayerFeesIpn', errmsg);
		ipnData.refCode = 'IPN';		// force a dummy value so that the user event web service does not resend the VB
	}

	
	// a negative payer fee means an actual fee. And a positive payer fee is a vendor credit. This is a re-imbursement of fees. 
	try{
		vendorBill.setFieldValue('entity', ipnData.tipaltiVendorId);
		vendorBill.setFieldValue('currency', ipnData.currencyId);
		vendorBill.setFieldValue('exchangerate', 1.0);
		vendorBill.setFieldValue('approvalstatus', '2');
	//	vendorBill.setFieldValue('location', ipnData.defaultLocation);
	 //  	vendorBill.setFieldValue('custbody_ipn_generated_bill_tipalti', 'T');		// check the box to show that this vendor bill was from the IPN
	   	vendorBill.setFieldValue('custbody_ipn_generated_bill_tipalti', ipnData.refCode);		// set the reference code on the VB to link back to payment.
		vendorBill.selectNewLineItem('expense');			// must select the current line first before any edits can occur
		vendorBill.setCurrentLineItemValue('expense', 'amount', Math.abs(ipnData.payerFees) );			//NOTE - THIS WILL NEED TO CHANGED WHEN HANDLING CREDITS TO FEES
		vendorBill.setCurrentLineItemValue('expense', 'account', ipnData.payerFeeAccount);
		vendorBill.commitLineItem('expense');
	} catch (e) {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Vendor Bill ' + e.getCode() + ' ' + e.getDetails();
		nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
		return {"Response" : errmsg};	
	}	

	// submit vendor bill for payer fees. 
	if (vendorBill) {
		try {
	    	vendorBillId = nlapiSubmitRecord( vendorBill );
	    	errmsg = 'Vendor Bill insert successful ID = ' + vendorBillId;
			nlapiLogExecution('DEBUG', 'tipalti processPayerFeesIpn', errmsg);
	    } catch (e) {
	    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
			nlapiLogExecution('ERROR', 'tipalti processPayerFeesIpn', errmsg);
			// rollback the vendor credit
			return {"Response" : errmsg};	
	    }	
	    // now create the corresponding payment so that the payer fee vendor bill becomes fully paid.
	    // the actual process is that payees will have already paid the fees to Tipalti prior to the invoice being created.
		// record must be created in dynamic mode so that the apply sublist can be pre-populated with all VBs and VCs for that Vendor
		var payment = nlapiCreateRecord('vendorpayment', {recordmode: 'dynamic', entity: ipnData.tipaltiVendorId});		
		
		if (payment == null) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not create Vendor Payment transaction.';
			nlapiLogExecution('ERROR', 'tipalti processPayerFeesIpn', errmsg);
			return {"Response" : errmsg};
		}
		
		// set all of the required header values
//		payment.setFieldValue('apacct', apAcctId );
		payment.setFieldValue('currency', ipnData.currencyId);
		payment.setFieldValue('trandate', dString);
//		payment.setFieldValue('externalid', ipnData.refCode);
		payment.setFieldValue('custbody_refcode_tipalti', ipnData.refCode);
		payment.setFieldValue('tranid', ipnData.refCode);				// set the check # = to refcode as well.
		payment.setFieldValue('exchangerate', 1.0);
		payment.setFieldValue('account', bankId);
		// loop through all of the vendor bills listed on the IPN
		errmsg = 'New payment apply count = ' +  payment.getLineItemCount('apply');
		nlapiLogExecution('DEBUG', 'tipalti processPayerFeesIpn', errmsg);
		
		var found = false;
		// find the matching VB that was just created in the apply sublist via internal IDs.
		for (var j = 1; j <= payment.getLineItemCount('apply'); j++) {
			errmsg = 'Matching Payer Fee VB = ' + vendorBillId + ' Apply VB ID[' + j + '] = ' + payment.getLineItemValue('apply', 'internalid', j);
			nlapiLogExecution('DEBUG', 'tipalti processPayerFeesIpn', errmsg);
			if (payment.getLineItemValue('apply', 'internalid', j) == vendorBillId) {
				found = true;
				payment.selectLineItem('apply', j);			// must select the current line first before any edits can occur
				payment.setCurrentLineItemValue('apply', 'apply', 'T');
				payment.commitLineItem('apply');
				break;
			}
		}
		if (!found) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Could not find vendor bill = ' + vendorBillId + ' for this Apply list.';
			nlapiLogExecution('ERROR', 'tipalti processPayerFeesIpn', errmsg);
			return {"Response" : errmsg};			
		}
		
		try {
	    	var paymentId = nlapiSubmitRecord( payment );
	    	errmsg = 'Vendor Payment for Fees insert successful ID = ' + paymentId;
			nlapiLogExecution('DEBUG', 'tipalti processPayerFeesIpn', errmsg);
	    } catch (e) {
	    	errmsg = e.getCode() + ' ' + e.getDetails();
			nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
			if (vendorBillId != 0) {
				try {
					nlapiDeleteRecord('vendorbill', vendorBillId);
			    } catch (e) {
			    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
					nlapiLogExecution('ERROR', 'tipalti processSubmittedIpn', errmsg);
					return {"Response" : errmsg};	
			    }	
			}
			return {"Response" : errmsg};	
	    }	

	    
	    
	}
	return {"Response" : "OK"};	

}

/*
 * This function will upsert an invoice into NetSuite. It assumes that the vendor exists. It will do a search on the reference code
 * to determine if the VB already exists. If not, it will create a new transaction. It also validates each GL account number on the 
 * transaction lines. 
 * 
 */

function processInvoice(ipnData) {
	var errmsg;
	var currentContext = nlapiGetContext();
	var newRecord = false;
	var vbRecord = null;
	// validate the approval status
	switch (ipnData.approvalStatus) {
	case 'PendingApproval':
	case 'PendingReview':
	case 'PendingApAction':
		ipnData.approvalStatus = '1';
		break;	
	case 'PendingPayment':
	case 'SubmittedForPayment':
	case 'SubmittedToPayment':
	case 'Paid':
		ipnData.approvalStatus = '2';
		break;
	case 'Deleted':
	case 'Disputed':
		ipnData.approvalStatus = '3';			// rejected status
		break;
	default:
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid approval status = ' + ipnData.approvalStatus;
		nlapiLogExecution('ERROR', 'tipalti processInvoice', errmsg);
		return {"Response" : errmsg};	
	}
	var vbFilter = new Array();
	vbFilter[0] = new nlobjSearchFilter( 'entity', null, 'anyof', ipnData.vendorId);
	vbFilter[1] = new nlobjSearchFilter( 'custbody_refcode_tipalti', null, 'is', ipnData.refCode);
	var vbColumn = new Array();
	vbColumn[0] = new nlobjSearchColumn( 'approvalstatus' );
	var vbResults = nlapiSearchRecord( 'vendorbill', null, vbFilter, vbColumn );
	errmsg = 'IPN approval status = ' + ipnData.approvalStatus;
	if (vbResults == null) {
		if (ipnData.approvalStatus == '3') {
			errmsg = 'Could not find Vendor Bill ' + ipnData.refCode + ' which is being rejected. IPN status = ' + ipnData.approvalStatus;
			nlapiLogExecution('ERROR', 'tipalti processInvoice', errmsg);
			return {"Response" : errmsg};		
		}
		vbRecord = nlapiCreateRecord('vendorbill', {recordmode: 'dynamic'});
		vbRecord.setFieldValue('entity', ipnData.vendorId);
		vbRecord.setFieldValue('custbody_refcode_tipalti', ipnData.refCode);
		vbRecord.setFieldValue('custbody_ipn_generated_bill_tipalti', ipnData.refCode);		// set the reference code on the VB to link back to payment.
		vbRecord.setFieldValue('currency', ipnData.currencyId);
		vbRecord.setFieldValue('approvalStatus', ipnData.approvalStatus);
		newRecord = true;
	} else {
		vbRecord = nlapiLoadRecord('vendorbill', vbResults[0].getId());	
		errmsg += ' VB record status = ' + vbRecord.getFieldValue('approvalstatus');
		if (vbRecord.getFieldValue('approvalstatus') != '2') {		// can only update non-approved VBs
			vbRecord.setFieldValue('approvalstatus', ipnData.approvalStatus);			
		}
	}
	nlapiLogExecution('DEBUG', 'tipalti processInvoice', errmsg);
	var tempDate = nlapiStringToDate(ipnData.tranDate, 'datetime');			// Tipalti sends dates in datetime format. Need to strip the time.
	vbRecord.setFieldValue('trandate', nlapiDateToString(tempDate, 'date'));
	tempDate = nlapiStringToDate(ipnData.dueDate, 'datetime');			// Tipalti sends dates in datetime format. Need to strip the time.
	vbRecord.setFieldValue('duedate', nlapiDateToString(tempDate, 'date'));
	vbRecord.setFieldValue('tranid', ipnData.tranId);
	vbRecord.setFieldValue('memo', ipnData.memo);
	
	// loop through all of the input lines. For new records, just create new line items.
	if (newRecord) {
		for (var i in ipnData.lines) {
			try {
				vbRecord.selectNewLineItem('expense');			// must select the current line first before any edits can occur
				vbRecord.setCurrentLineItemValue('expense', 'amount', Math.abs(ipnData.lines[i].amount) );	
				vbRecord.setCurrentLineItemValue('expense', 'account', ipnData.lines[i].account);
				vbRecord.setCurrentLineItemValue('expense', 'memo', ipnData.lines[i].memo);
				if (ipnData.lines[i].classification != '')
					vbRecord.setCurrentLineItemValue('expense', 'class', ipnData.lines[i].classification);
				if (ipnData.lines[i].department != '')
					vbRecord.setCurrentLineItemValue('expense', 'department', ipnData.lines[i].department);
				if (ipnData.lines[i].location != '')
					vbRecord.setCurrentLineItemValue('expense', 'location', ipnData.lines[i].location);
				vbRecord.commitLineItem('expense');
			} catch (e) {    	
		    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
				nlapiLogExecution('ERROR', 'tipalti processInvoice', errmsg);
				return {"Response" : errmsg};		
			}
		}
	} else {
		// note - cannot use the FOR IN syntax due to javascript treating i as a string.
		for (var i = 0; i < ipnData.lines.length; i++) {
			var found = false;
			for (var j = 1; j <= vbRecord.getLineItemCount('expense'); j++) {
//				errmsg = 'i = ' + i + ' j = ' + j 
//					+ ' line value = ' + vbRecord.getLineItemValue('expense', 'line', j) 
//					+ ' amount = ' + vbRecord.getLineItemValue('expense', 'amount', j);
//				nlapiLogExecution('DEBUG', 'tipalti processInvoice', errmsg);
				if (parseInt(vbRecord.getLineItemValue('expense', 'line', j)) == (i + 1)) {
					errmsg = 'Found existing line = ' + vbRecord.getLineItemValue('expense', 'line', j);
					nlapiLogExecution('DEBUG', 'tipalti processInvoice', errmsg);
				
					vbRecord.setLineItemValue('expense', 'amount', j, Math.abs(ipnData.lines[i].amount) );	
					vbRecord.setLineItemValue('expense', 'account', j, ipnData.lines[i].account);
					vbRecord.setLineItemValue('expense', 'memo', j, ipnData.lines[i].memo);
					if (ipnData.lines[i].classification != '')
						vbRecord.setLineItemValue('expense', 'class', j, ipnData.lines[i].classification);
					if (ipnData.lines[i].department != '')
						vbRecord.setLineItemValue('expense', 'department', j, ipnData.lines[i].department);
					if (ipnData.lines[i].location != '')
						vbRecord.setLineItemValue('expense', 'location', j, ipnData.lines[i].location);
					found = true;
					break;
				}
			}
			if (!found) {		// new line item on the VB
				errmsg = 'New line = ' + i + ' account = ' + ipnData.lines[i].account + ' amount = ' + ipnData.lines[i].amount;
				nlapiLogExecution('DEBUG', 'tipalti processInvoice', errmsg);

				vbRecord.selectNewLineItem('expense');			// must select the current line first before any edits can occur
				vbRecord.setCurrentLineItemValue('expense', 'amount', Math.abs(ipnData.lines[i].amount) );	
				vbRecord.setCurrentLineItemValue('expense', 'account', ipnData.lines[i].account);
				vbRecord.setCurrentLineItemValue('expense', 'memo', ipnData.lines[i].memo);
				if (ipnData.lines[i].classification != '')
					vbRecord.setCurrentLineItemValue('expense', 'class', ipnData.lines[i].classification);
				if (ipnData.lines[i].department != '')
					vbRecord.setCurrentLineItemValue('expense', 'department', ipnData.lines[i].department);
				if (ipnData.lines[i].location != '')
					vbRecord.setCurrentLineItemValue('expense', 'location', ipnData.lines[i].location);
				vbRecord.commitLineItem('expense');
			}
		}

	}

	try {
		currentContext.setSessionObject('Calling Script', 'IPN RESTlet');// set param for sessions. for correct passing. helps with communications between schedule and user event scripts.
    	var vbId = nlapiSubmitRecord( vbRecord );
    	errmsg = 'Vendor Bill insert successful ID = ' + vbId;
		nlapiLogExecution('DEBUG', 'tipalti processInvoice', errmsg);
    } catch (e) {
    	errmsg = 'Error: IPN Refcode ' + ipnData.refCode + e.getCode() + ' ' + e.getDetails();
		nlapiLogExecution('ERROR', 'tipalti processInvoice', errmsg);
		return {"Response" : errmsg};	
    }	

	return {"Response" : "OK"};	
}

/*
 * This function will look up all vendor bills associated with the payment. Note - You need the 'Transactions -> Find Transaction' permission to do the search.
 * Originally this function worked with external IDs and was able to return just the vendor bills in the IPN list. 
 * But with using the custom text field, the anyof search filter does not work. So this function now loops through the
 * entire list of vendor bills for the vendor. It then builds an array with the internal ID and amount for each matching VB.
 * It also determines if the AP account for the vendor is valid for the integration using the custom checkbox on the account definition
 * 
 */


function searchVB(vbArray, vendorId) {
	var errmsg;
	var vbList = new Array();
	var ctr = 0;
	
	var vbFilter = new Array();
	vbFilter[0] = new nlobjSearchFilter( 'entity', null, 'anyof', vendorId);
	vbFilter[1] = new nlobjSearchFilter( 'mainline', null, 'is', 'T');
	// return the approval status and the transaction type
	var vbColumn = new Array();
	vbColumn[0] = new nlobjSearchColumn( 'custbody_refcode_tipalti' );
	vbColumn[1] = new nlobjSearchColumn( 'amount' );
	vbColumn[2] = new nlobjSearchColumn( 'account' );
	vbColumn[3] = new nlobjSearchColumn( 'custrecord_apacct_tipalti', 'account' );
	vbColumn[4] = new nlobjSearchColumn('internalid');
	vbColumn[4].setSort(false);
	errmsg = 'Before search Vendor ID = ' + vendorId;
	nlapiLogExecution('DEBUG', 'searchVB', errmsg);

	// there may be > 1000 rows for search results on VBs per vendor. Need to concatenate a series of searches together. 
	var tempSearchresults = nlapiSearchRecord( 'vendorbill', null, vbFilter, vbColumn );
	var vbResults = tempSearchresults // copies the result, this is the main bucket of results.
	while(tempSearchresults !=null && tempSearchresults.length == 1000){
	   var lastId = tempSearchresults[999].getId(); //the last record before 1000
	   vbFilter.push (new nlobjSearchFilter('internalidnumber',null,'greaterthan',lastId) ); //create new filter to restrict the next search based on the last record returned )
	   tempSearchresults = nlapiSearchRecord( 'vendorbill', null, vbFilter, vbColumn );
	   vbFilter.pop();
	   if (tempSearchresults)
		   vbResults = vbResults.concat(tempSearchresults); //add the result to the complete result set 
	}
	if (vbResults) {
		errmsg = 'search result count' + vbResults.length;
		nlapiLogExecution('DEBUG', 'searchVB', errmsg);		
		for (var i = 0; i < vbResults.length; i++) {
			var vbResult = vbResults[i];
			if (vbResult) {
//				only debug for results < 1000				
//				errmsg = 'search result vb ref code[' + i + '] = ' + vbResult.getValue('custbody_refcode_tipalti') + ' AP account = ' + vbResult.getValue('account');
//				nlapiLogExecution('DEBUG', 'searchVB', errmsg);		
				if (vbArray.indexOf(vbResult.getValue('custbody_refcode_tipalti')) >= 0) {
					if (vbResult.getValue( 'custrecord_apacct_tipalti', 'account' ) != 'T')	{		// invalid AP account on vendor bill
						errmsg = 'Invalid AP account used for this vendor bill. Account = ' + vbResult.getValue( 'account' ) 
							+ ' flag = ' + vbResult.getValue( 'custrecord_apacct_tipalti', 'account' );
						nlapiLogExecution('ERROR', 'searchVB', errmsg);
						return null;
					}
					vbList[ctr] = new Object();
					vbList[ctr].internalId = vbResult.getId();			
					vbList[ctr].amount = vbResult.getValue('amount');
					vbList[ctr].apAccount = vbResult.getValue('account');
					ctr++;
				}
			}
		}
	} else {
		return null;
	}
//	for (var i = 0; i < vbList.length; i++) {
//		errmsg = 'Matched VB[' + i + '] = ' + vbList[i].internalId + ' amount = ' + vbList[i].amount + ' AP account = ' + vbList[i].apAccount;
//		nlapiLogExecution('DEBUG', 'searchVB', errmsg);
//	}
	return vbList;
}

/*
 * This function verifies that the vendor on the IPN exists in NetSuite. It will do a search by the custom payeeid field. 
 * If this doesn't work, it searches by external ID passed on the IPN. It saves the subsidiary of the vendor.
 * 
 */

function searchVendor(ipnData) {
	var errmsg;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter( 'custentity_payeeid_tipalti', null, 'is', ipnData.vendorName );
	filters[1] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F' );
	var columns = new Array();
	columns[0] = new nlobjSearchColumn(  'subsidiary' );
//	columns[1] = new nlobjSearchColumn(  'custentity_payeeid_tipalti' );
	var searchResults = nlapiSearchRecord( 'vendor', null, filters, columns);
//	errmsg = 'initial payeeid search results = ' + ipnData.vendorName + ' '+ searchResults ;
//	nlapiLogExecution('DEBUG', 'tipalti searchVendor', errmsg);
//	for (var i in searchResults) {
//		var result = searchResults[i];
//		errmsg = 'Loop[' + i + '] = ' + result.getId() + ' ' + result.getValue('custentity_payeeid_tipalti');
//		nlapiLogExecution('DEBUG', 'tipalti searchVendor', errmsg);
//	}
	
	if (searchResults) {
		ipnData.vendorId = searchResults[0].getId();
		ipnData.subsidiary = searchResults[0].getValue('subsidiary');		
//		errmsg = 'first vendor id = ' + ipnData.vendorId + searchResults[0].getValue('custentity_payeeid_tipalti');
//		nlapiLogExecution('DEBUG', 'tipalti searchVendor', errmsg);
//		if (searchResults[0].getValue('custentity_payeeid_tipalti') == ipnData.vendorName) {
//			errmsg = 'Match found';
//			nlapiLogExecution('DEBUG', 'tipalti searchVendor', errmsg);
//		}
	} else {
		filters[0] = new nlobjSearchFilter( 'entityid', null, 'is', ipnData.externalId );
		searchResults = nlapiSearchRecord( 'vendor', null, filters, columns);
//		errmsg = 'externalid search results = ' + searchResults + ' external id = ' + ipnData.externalId + '||';
//		nlapiLogExecution('DEBUG', 'tipalti searchVendor', errmsg);
		if (searchResults) {
			ipnData.vendorId = searchResults[0].getId();
			ipnData.subsidiary = searchResults[0].getValue('subsidiary');		
//			errmsg = 'external id search = ' + ipnData.vendorId;
//			nlapiLogExecution('DEBUG', 'tipalti searchVendor', errmsg);
//			ipnData.apAccount = searchResults[0].getValue('payablesaccount');
		} else {		
			ipnData.vendorId = null;
		}
	}
	errmsg = 'Result vendor id = ' + ipnData.vendorId + ' sub = ' + ipnData.subsidiary;
	nlapiLogExecution('DEBUG', 'tipalti searchVendor', errmsg);
	
}

function searchBank(ipnData) {
	var bankId = 0;
	var errmsg = 'OK';
	// validate the bank account info. Bank account is required and it must exist in NetSuite based upon the custom field value
	if (ipnData.bankName == '') {
		errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Missing Bank Account';
		nlapiLogExecution('ERROR', 'tipalti searchBank', errmsg);

		return {"Response" : errmsg};
	} else {
		// first look up bank name in the custom record of allowable bank names. The corresponding internal ID will be used on the account definition
		var bankFilter = new Array();
		bankFilter[0] = new nlobjSearchFilter( 'name', null, 'is', ipnData.bankName);

		var bankResults = nlapiSearchRecord( 'customrecord_bankname_tipalti', null, bankFilter, null );
		if (bankResults == null) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Bank Name = ' + ipnData.bankName;
			nlapiLogExecution('ERROR', 'tipalti searchBank', errmsg);
			return {"Response" : errmsg };	
		}
		// Now look up the GL account that is using this bank account.
		var acctFilter = new Array();
		acctFilter[0] = new nlobjSearchFilter( 'custrecord_bankacct_tipalti', null, 'anyof', bankResults[0].getId());

		var acctResults = nlapiSearchRecord( 'account', null, acctFilter, null );
		if (acctResults == null) {
			errmsg = 'Error: IPN Refcode ' + ipnData.refCode + ' Invalid Bank Account = ' + ipnData.bankName;
			nlapiLogExecution('ERROR', 'tipalti searchBank', errmsg);
			return {"Response" : errmsg };	
		}
		bankId = acctResults[0].getId();
		errmsg = 'Bank Account ID = ' + bankId;
		nlapiLogExecution('DEBUG', 'tipalti searchBank', errmsg);
		return bankId;
	}
	
}

/*
 * This function validates segments passed in on a vendor bill line. It is generic in that the input object name is dynamically referenced.
 * Input parameters
 * 	dataInValue - data input value from Tipalti. 
 * 	searchResults - list of all available account types.
 * 	segmentName - string value of the segment name.
 *  numberFlag = true when searching by account number, = false when searching by name field for any segment.
 */

function searchSegment(dataInValue, searchResults, segmentName, numberFlag) {
	var errmsg = 'OK';
	var found = false;
	for (var i in searchResults) {
		var result = searchResults[i];
		errmsg = segmentName + ' data input = ' + dataInValue + ' search result[' + i + '] = ' + result.getValue('name') + ' ID = ' + result.getId();
		nlapiLogExecution('DEBUG', 'tipalti searchSegment', errmsg);
		if (numberFlag) {
			if (dataInValue.toLowerCase() == result.getValue('number').toLowerCase()) {
				return result.getId();
			}		
		} else {
//			if (dataInValue.toLowerCase() == result.getValue('name').toLowerCase()) {			// 11-3-16 switched to using internal ID that was saved in Tipalti
			if (dataInValue.toLowerCase() == result.getId()) {
				return result.getId();
			}
		}
	}
	if (!found) {
		errmsg = 'Invalid ' + segmentName + ' = ' + dataInValue;
		nlapiLogExecution('ERROR', 'tipalti searchSegment', errmsg);
		return {"Response" : errmsg };			

	}

//	return {"Response" : "OK" };				
}
