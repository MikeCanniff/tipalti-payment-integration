/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Mar 2016     Keely
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function tiplatiScheduledVendorBillSync(type) {
	var govLimit = 200;               // let the governance wind down from 10000 to 200 for this scheduled script
	var currentContext = nlapiGetContext();
	var msg = ' script is executing ';
	var paramMsg = '';	
	var endDate = currentContext.getSetting('SCRIPT', 'custscript_enddate_tipalti');  // grabbing the params from NS
	var startDate= currentContext.getSetting('SCRIPT', 'custscript_startdate_tipalti');		
	//var startDate= currentContext.getValue('SCRIPT', 'trandate');	
	paramMsg = ' endDate = ' + endDate + ' startDate = ' + startDate;
	nlapiLogExecution('DEBUG', ' tiplatiScheduledVendorBillSync ', paramMsg);
	var typeArray = new Array(); //push all in there
	if((startDate == null || startDate == '')  &&  (endDate == null || endDate == '') ) 
	{
		resultmsg = 'cannot find any parameters';
		nlapiLogExecution('ERROR', ' tiplatiScheduledVendorBillSync ', resultmsg);
	}else {
		var filters = new Array();
		filters.push ( new nlobjSearchFilter( 'trandate', null, 'notbefore', startDate ) ); //picks up the vendor created within certain dates
		filters.push (new nlobjSearchFilter( 'trandate', null, 'notafter', endDate ) );
		//filters.push (new nlobjSearchFilter ('custentity_payeeid_tipalti', null, 'isempty'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'custentity_payeeid_tipalti' );  // this is here to grab the information later for if logic

		var searchresults = nlapiSearchRecord( 'vendor', null, filters, columns ); // go to dB find all records match. 1st type, 3rd critera, col= what info from record etc
		for ( var i = 0; searchresults != null && i < searchresults.length; i++ ){ // to get the info from records
			var searchresult = searchresults[ i ];
			var tipaltiPayeeId = (searchresult.getValue('custentity_payeeid_tipalti' ));
			var vendorId = searchresult.getId();	// get the id of the specific vendor
			var recType = searchresult.getRecordType(); // get the recordtype of the specific vendor type
			msg = 'tipaltiPayeeId = ' + tipaltiPayeeId+ ' vendorId = ' + vendorId + ' recType = ' +recType;
			nlapiLogExecution('DEBUG', ' tiplatiScheduledVendorBillSync  ', msg);

			if(  tipaltiPayeeId == null || tipaltiPayeeId == '' ) // this makes sure that only the vendors with the blank field gets updated.
			{
				//try{
					var vendor = nlapiLoadRecord ( recType, vendorId ); // loading the record
					if( vendor != null ){
						nlapiLogExecution('DEBUG',' tiplatiScheduledVendorBillSync ', 'vendor name = ' + vendor.getFieldValue('companyname') );
						vendor.setFieldValue('custentity_payeeid_tipalti','' );			
						var update = nlapiSubmitRecord(vendor ); 		
						var internalMsg = 'initial sync in process, update complete ';
						nlapiLogExecution('DEBUG',' tiplatiScheduledVendorBillSync ', internalMsg);
					}

				//}
				//catch(e)
				//{
				//	msg = 'failed vendor payee id update' + e;
				//	nlapiLogExecution('ERROR',' tiplatiScheduledVendorSync ', msg);
				//}
			}
			if( currentContext.getRemainingUsage() < govLimit ) {  
				var state = nlapiYieldScript();
				if( state.status == 'FAILURE')   {
					errmsg = "Failed to yield script, exiting: Reason = " + state.reason + " / Size = " + state.size;
					nlapiLogExecution('ERROR', ' tiplatiScheduledVendorBillSync  ', errmsg);
					return;
				} 
				else if ( state.status == 'RESUME' ){
					errmsg = "Resuming script because of " + state.reason + ".  Size = " + state.size;
					nlapiLogExecution('AUDIT', ' tiplatiScheduledVendorBillSync  ', errmsg);
				}
				// state.status will never be SUCCESS because a success would imply a yield has occurred.  The equivalent response would be yield
			} // end if govLimit 

		}// end for loop
	}
	nlapiLogExecution('DEBUG',' tiplatiScheduledVendorSync ', ' at the end of the function tiplatiScheduledVendorSync ');


}
function runsyncProcess(){  // this is excuted when the button (Sync Vendors) is clicked on. 
	var post = new Array();
	alert(' tipalti vendor sync Process has been submitted ');
	post['endDate'] = nlapiGetFieldValue ('custrecord_vendorsyncenddate_tp');
	post['startDate'] = nlapiGetFieldValue('custrecord_vendorsyncstartdate_tp');

	var resolve = nlapiResolveURL('SUITELET', 'customscript_vendorsyncsuitelet_tp', 'customdeploy_vendorsyncsuitelet_tp');   
	var request = nlapiRequestURL( resolve ,post);
}